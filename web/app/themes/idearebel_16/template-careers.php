<?php
/**
 * Template Name: Careers Page Template
 */
?>
<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'careers'); ?>
<?php endwhile; ?>