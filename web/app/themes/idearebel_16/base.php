<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

$original_id = get_the_ID();

?>

<!doctype html>
<html <?php language_attributes(); ?>>

  <?php get_template_part('templates/head'); ?>

  <body <?php body_class(); ?>>
  
    <!--[if lt IE 9]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->

    <?php
    do_action('get_header');
    get_template_part('templates/header');
    ?>

    <div id="page-id-<?php echo $original_id ?>" class="ir-wrap wrap container" role="document">

      <div class="ir-content ir-content-padding content row">

        <main class="ir-main main page-<?php echo get_post_type();?>">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->

        <?php if (Setup\display_sidebar()) : ?>
          <aside class="sidebar">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>

      </div><!-- /.content -->

    </div><!-- /.wrap -->

    <?php
    do_action('get_footer');
    get_template_part('templates/footer');
    wp_footer();
    ?>
    
  </body>
</html>
