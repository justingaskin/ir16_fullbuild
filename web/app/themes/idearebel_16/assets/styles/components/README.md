## Components folder
## http://docssa.info/#components

The "components" folder is where your ui-components are located. It is the place dedicated to store your reusable UI appearance.

Components are an important part of DoCSSa. They have been redesigned since version 1 of DoCSSa to become simpler and easier to use. They are the "Deferred Object" part of DoCCSa.

We say the Object is Deferred because its core resides in an abstraction (the mixin) instead of being tied to a classname. DoCSSa's components are class agnostic. They are made of mixins. A component mixin is in charge of binding the reusable styling to one or several HTML class(es). Thanks to the BEM naming convention, the class passed to the component mixin can be a prefix for other classes onto which to bind functionalities.

This allows for components to be instantiated on any class, and to be composable if needed.

As the component is not tied to a particular class, you can use it on whatever class you want, whenever you want, from the stylesheet side. That means that you can (and should) keep a semantic meaning to your HTML classes, and change their look and feel without having to modify the markup.

When your markup comes from a RTE, this is a huge gain. You can change the styling without asking the contributors to change their habits, and you can affect everything already written without having to make search&replace throughout a database or without having a "red" class helper mean the color is going to be purple!

For example, instead of having a link with the "button red" classes, you can give it a "navTrigger" class and bind a button component with a red skin to it. You could also use the same component on a "submitTrigger" class so that both look the same. When time comes to have a different look for your submits, all you have to do is bind the submitTrigger to another component and you're done, without affecting the navTriggers and without touching the markup.

Note that for advanced components, the HTML may be expected to conform to a particular structure to be a valid target.

If you need a component with a slightly different behaviour than the original one, you can pass a parameter to the mixin to change the binding (or rules). If the exceptions become too numerous, it's probably time to consider creating another component or a set of small components.

In order to fulfill their role, components need to respect those two guidelines :

* A component should be self contained.
This means that what is outside of the visual boundaries of the component doesn't belong in the component definition. Typically, things like margins or positioning should reside in the "specifics" folder, not in the component. This is required for your component to live in any possible context.

* Structure(layout) should be dissociated from skin(paint).
For example, background color, images, text colors, etc. may go into the skin section of the component, not in its core definition. That way, you can create additional styles for them without altering their expected behaviour as a component, and choose what visual style you want when binding the component to a classname.

Of course, it's up to you to compose with those precepts and adapt them to your constraints, but respecting them results in a clean separation of concerns and genuinely reusable components, which is much likely to make your dev faster further down the road.

When beginning with DoCSSa, it's easy to think as everything as components, as they are so powerful. You must be careful about that, and think about what needs to be a component and what doesn't.

If you component-ize every set of rules, you risk spending more time building the same thing as you would have in the "specifics" folder without enough additional value for it to be worth it. Try to begin with small components to get the hang of it, and adjust your use little by little.

You can begin everything the way you are used to in the "specifics" folder, organize it in imported files and subfolders the DoCSSa way, and only extract a functionality to a component when you feel that it would help you.
DoCSSa only gives you tools to play with, what you implement and how you do things with it is up to you. That's what we believe a "framework" should be anyway.

When looking at a component folder, you will see one file, and possibly some subfolders. Let's take a Tabs component as an example, as probably everyone has already seen a tabbed navigation in a page. The file in the Tabs folder would be: _tabs.scss.
It is the component definition. Let's look at it in details.

## Component's definition

The component definition file contains two mixins. One for the component structure, one for the component's skin.

A mixin named after the component is all it takes. Its role is to actually bind the component's styling rules to the provided classname, its "elements", and its state.

We recommend that the "structure" part of the component doesn't make use of any project variable in order to stay as generic and reusable as possible.

By default, the component binding will also implement the default skin. If we have defined another skin, all we need to do is pass "$defaultSkin: false" when instantiating the component, and call another skin binding mixin to handle the component's skin.

We said earlier that a component folder may have subfolders. One of those is a "_skins" folder that would host different skins that we could import as needed. The "_skins" folder is prefixed by an underscore only so that it always show on top of the list in case you need some other folders in there.

As you can see, it is very close to the structure part. Actually it is placed inside the component's folder for simplicity, but it could very well reside in a _skins folder right away.

As stated before, a component's "structure" should only contain it's layout, it's skeleton. The "skin" part, unlike the structure, should only contain rules that don't affect the structure. These rules can be background-colors, background-images, border-radius, shadows, opacity... Anything you want as long as it doesn't fiddle with the component's external boundaries.

It is not always easy, but the more you practice it the more it feels natural, and the easier it is to find out the right place when you come back to adjust something about your component.

Now that we know how a component is structured, it's time to implement it. This is done by calling the component mixin and passing it a class selector.

If later on you need to bind the tabs component to another class, all you have to do is to call the mixin with that class and the parameters you want, and you're done!


