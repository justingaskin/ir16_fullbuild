## Specifics folder

The "specifics" folder is your common playground. It is where you'll place the rules that don't belong in the "base" or "components" folders. Until you become fluent with DoCSSa's organization system, what will eventually end up in a component will probably exist in here first.

Specifics is the closest thing to your usual CSS file, except that everything in there is split and dispatched in files and folders according to what they apply to. This fragmentation is key to keeping things maintainable!

By convention, we're using two underscores __ as a prefix for files that act as an import summary, and only one underscore _ for files which are content only. This usually evolves with the project: you begin with an underscore prefixed file, you add another one, and at some point you stop calling them directly from the main .scss file and you reorganize them in a folder with its own summary file.

For example you might begin writing some rules in specifics/__specifics.scss, and as it grows realize that you have a bunch of rules all related to the popin windows. You'd then move those rules in a _popin.scss file and import that file from __specifics.scss.

After a while, you'll notice that you added some rules for a bunch of specific popins on your site. That would be a good time to create a "popins" folder and split the specific popin rules to different files in that folder (say, popins/_popin-loginForm.scss and popins/_popin-congratulations.scss).

Instead of importing _popins.scss from __specifics.scss, you'd then import popins/__popins.scss instead, and let this summary file import all the specific popins required for your project.

This allow you to have all your feature related files in the same folder, and one summary specific to that kind of feature which can import or leave out the files in its section as easily as commenting out a line. If at some point you realize that you don't want to use the congratulations popin anymore, all you have to do is to comment out the line importing its definition file (_popin-congratulations.scss) from the popins summary (popins/__popins.scss).

DoCSSa encourages you to keep your definitions tidy and reorganize in subfolders as soon as it makes sense.
No file should ever be big enough that you can't scroll through it in a few mousewheel movements max!

Before everyone in your team is familiar with DoCSSa, it can be helpful for occasional contributors wondering where to place their code to have a dedicated file. In such case, we recommend using a _inbox.scss file in the "specifics" folder and ask them to commit their work in there.

It shall be emptied regularly by a more experienced DoCSSa user, who would move the CSS rules defined in there to the correct place in the architecture.


