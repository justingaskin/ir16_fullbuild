* _variables is were all site wide variables reside. 
Default values, color theme, configuration variables, etc. go into this file.

* The _fonts file is used —you guessed it— for the font-families declaration.
In our implementation, we use a font mixin that is in charge of generating a bulletproof syntax according to the passed configuration for each needed font, according to a file naming convention.

But where do we have to place the font files themselves, you may be wondering? Well, as this Sass structure is intended to be compiled to CSS in a different directory, the fonts will be in that directory. Typically, you'll have the custom.scss file in a "sass" folder" compiled to a custom.css in a "css" or "styles" folder. The font files will have to be there (preferably in a "fonts" subfolder in order to stay nice and tidy).

Same goes for all the image files you may be referring to in your stylesheets.

* _mixins is a place for you to store simple helper that you may want to reutilize throughout the site, but are not generic enough to endup in the utils/_mixins file.

* _globals contains rules that are global to the site. Things like box-sizing type, html font size, body background color, headings and link defaults, etc. are defined here. It is also be a good place to store your layout definition if it is used for all pages on the site.

