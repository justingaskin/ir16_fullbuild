* _system is a special file that contains tools used by DoCSSa.
Most notably, it contains a mixin that helps requiring content from several places without generating duplicate content, and one that helps the provided components to make use of Modernizr classes.

* _mixins contains a collection of commonly used helpers. 
They are particularily useful for handling vendor prefixes and fallbacks. For example, DoCSSa comes with a linear-gradient mixin that compiles to all required vendor prefixes (and only those required!) and generates a fallback color from the input values for browsers that don't support linear-gradient at all. DoCSSa recommends using Modernizr for feature detection and progressive enhancement, and the provided mixins implementing CSS3 features rely on it by default for their output (even though it can be disabled at include time).

Mixins can accumulate and enrich your library with no cost, as they are only compiled when used.

Mixins are a great way to avoid repetition when coding. They end up in code repetition in the css output, and for that reason the previous version of DoCSSa was based on placeholders instead, but it has now been proven that this repetition is not an issue once gzip comes into play (more info)