## Base folder

The "base" folder contains rules that are global to the site. It is divided in two parts :

* utils
* project

Note that the base/_config.scss file is located directly in the base folder. It contains variables that are required by both the utils and the project. So far only the $baseFontSize fell in that category, but we still dedicated a file to it in order to be consistent.

Utils contains only things that don't need to change from a project to another. As of today, it consists of _system and _mixins.

Project is where every global setup of a project is stored. Whereas utils is intended to be kept and grow from a project to the next, project only stands true for a given project (although you may want to use it as a boilerplate from project to project). As of today, it consists of _variables, _fonts, _mixins and _globals.

