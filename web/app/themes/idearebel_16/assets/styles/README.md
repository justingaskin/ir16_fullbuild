## What is DoCSSa ?

DoCSSa is a CSS architecture and methodology to help structure rapid and maintainable stylesheets development.
It is intended for use in large, long lived sites, on which many frontend developers may be working over time.
The name stands for Deferred Object CSS Architecture.

## What are the key features of DoCSSa ?

* Clear and proven folder structure
* Separation of concerns between HTML and CSS
* Scalable and maintainable stylesheets
* Easy to setup and flexible
* Why does DoCSSa exist ?

As frontend web developers, we are living in a world of evergrowing complexity. What once was a relatively easy thing to do (putting aside browsers inconsistencies) has grown into a very complex, dynamic, everchanging reality.

So, we need flexible structures in order to be more efficient and less under pressure when the rough times come. The more work we can avoid by having defined the right tools and the right architecture ahead of time, the better.

DoCSSa is our attempt at an organization that aims toward a fast workflow that will keep the flexibility required for the long run.

## How does DoCSSa bring its benefits ?

DoCSSa is based on Sass, and leverages some of its key features to achieve its goal. Most notably, DoCSSa takes advantage of imports and mixins. It also suggests a file system organisation and a BEM based naming convention, and integrates the core ideas found in OOCSS and SmaCSS.

## Basics
## http://docssa.info/#fileStructure_base

In DoCSSa, the file system is divided in four main directories in the sass folder :

* base
* components
* specifics
* vendor

Each base folder has a specific role, and CSS rules for a particular set of elements will end up in one of these directories, according to their nature.

The structure has a single point of entry, which we'll call custom.scss. This file is located at the root of the sass folder.

In most cases, it will be the only file with no underscore(_) prefix: as you may know, the .scss to .css convertion process in Sass only converts files that don't have an underscore in front of them. Most of the files in our structure being imported by other files, they don't need to be rendered directly.

Only components may be rendered separately, in order to be able to dynamically load them if needed, but we'll get back to it.

The main custom.scss mostly acts as an aggregator for other files, which themselves import some other scss files, and so on. With a well thought organization, this simple construct can prove very powerful!

* On a sidenote, remember that Sass is only a CSS precompiler, so you'll end up with only one .css file with all the goodies, not a bunch of HTTP requests.

## HTML classes
## http://docssa.info/#namingConventions

In DoCSSa, we decided to follow BEM class naming convention.

HTML class names for Blocks are lowerCamelCase, and the Elements nested within are separated by an underscore(_). Modifiers are separated by a double dash(--), and they are used for elements variants. An element's state is written separately with a pattern that begins with "_is_".

Variants are dissociated from states because they play a different role, and it appeared that with our components architecture, having a simple "_is_current" class for our items' "current" state was way more effective than a "mainMenu_item--current" when it came to modifying the state through javascript.

BEM modifiers are now exclusively used for visual variants that don't change over time. They have a meaning at the content(HTML) level. For example, if a tab needs to be highlighted so that it stands out, it is applied a variant by using a modifier.
By opposition, a "state" is something that is supposed to be handled by javascript and change over time.

At first, the BEM notation style can seem quite verbose and put off some developers, but it is very powerful and allows us to go way behind old school naming conventions.

It has to be tamed, though, and a classic beginner's mistake is to reflect the position of each element in the component's DOM in its class name. To avoid that, we recommend that you look at your low level elements first (those deeper in the DOM tree) and wonder if they could exist elsewhere in the page or in the component. Going up the tree, you can identify your most basic reusable components and set the names accordingly.

For example, in our example above, the "mainMenu_link" class doesn't need to reflect the fact that it is contained in a "mainMenu_item", this doesn't really matter to the link, so there is no reason to specify it in its name.

Also, if the element can live outside the block you're looking at, it's probably not a good idea to tie it to it, and it probably should exist on its own.

## Sass files

We talked about some of the aspects of the Sass file naming convention in the "File Structure" section.

Each file is lowerCamelCase named and prefixed by an underscore(_) so that it is considered by the Sass compiler as a partial and not rendered to a standalone CSS file.

Each folder has a single entry point in charge of importing other partial Sass files. This file is named after the folder's name, and is prefixed with two underscores(__) so that it always appears on top in the files list. It imports only the files in its own folder, except for other summary indexes located in a direct subfolder. This way, you always can handle your imports very easily and stop the imports of nested files at any level.

Components definition files should have the same name as the component's folder name, and have a single underscore prefix, as they are content and not imports list.

## SASS variables

Sass variable names should be composed of dash(-) separated parts, with each part sorted from the most generic to the most specific variable characteristic. This is quite useful in most IDE as you can take advantage of autocompletion.

## How to start?

* First, you can simply write CSS code in separated files into the "specifics" folder, using DoCSSa's logic. Once you feel comfortable with DoCSSa start writing a component and let the whole team know they can use it.

* Then, try manipulating the provided "_mixins" from the base/utils folder to give your project more reusable code.

* After that, you may write your own "_mixins", tailored for your own specific needs.

* For more reusability within or across your project(s), you may then try to convert some of your "specifics" rules into "components". If see you are repeating the same rules on different sets of classes, you have a good candidate.

