## Vendor folder

The vendor folder is where we import SCSS files that come from third parties and can be updated at any time. It can import from a bower folder, or have its assets directly in the folder. As the .scss syntax is CSS compatible, all we have to do is to rename the .css files to .scss, in order for them to be integrated to the .css compilation instead of referenced by the CSS file as a classic @import.

* _normalize.scss is an excellent candidate for this section, along with more project specific third party CSS. We currently rely on bower for fetching it for us.