// (function($) {
// 	//TODO: Need to add deceleration on scroll???
	
//     var delay = 0;
//     $.fn.translate3d = function(translations, speed, easing, complete) {
//         var opt = $.speed(speed, easing, complete);
//         opt.easing = opt.easing || 'ease-in-out';
//         translations = $.extend({x: 0, y: 0, z: 0}, translations);

//         return this.each(function() {
//             var $this = $(this);

//             $this.css({ 
//                 transitionDuration: opt.duration + 'ms',
//                 transitionTimingFunction: opt.easing,
//                 transform: 'translate3d(' + translations.x + 'px, ' + translations.y + 'px, ' + translations.z + 'px)'
//             });

//             setTimeout(function() { 
//                 $this.css({ 
//                     transitionDuration: '0s', 
//                     transitionTimingFunction: 'ease'
//                 });

//                 opt.complete();
//             }, opt.duration + (delay || 0));
//         });
//     };
    
//     function updateParallaxItems(duration) {
//     	var adjustmentCoefficient = 0.10; // A percentage scroll value based off of the containers height.
//   		var floatDistance = $(window).width() * 0.1;   // A static scroll value
//   		var animationSpeed = duration;
//   		var maxHeightPercentage = 2.0;
// 	  	var scrollDistanceToTop = $(window).scrollTop() + ($(window).height() / 2.0);
	  	
// 	  	if(!(window.matchMedia( "screen and (max-width: 767px)" ).matches)) {
// 		  	//Modify Position for each floating container
// 		  	$('.ir-floating-container').each(function (e) {
// 		  		//Modify Each Floating item in the floating container.
// 		  		$(this).find('.ir-floating-item').each(function (e) {
// 		  			var topOffset = $(this).offset().top;
// 		  			var height = $(this).parent().height();
// 		  			var maxFloatDistance = floatDistance;
// 		  			// var maxFloatDistance = height * adjustmentCoefficient;
		  			
// 		  			var topDistance = topOffset - scrollDistanceToTop;
// 		  			var topDistancePercent = topDistance / height; // -200% to 200%
// 		  			topDistancePercent = ((topDistancePercent > maxHeightPercentage) ? maxHeightPercentage : topDistancePercent);
// 		  			topDistancePercent = ((topDistancePercent < -maxHeightPercentage) ? -maxHeightPercentage : topDistancePercent);
// 		  			topDistance = topDistancePercent * maxFloatDistance;
		  			
// 		  			$(this).stop().translate3d({ x: 0, y: topDistance, z: 0}, animationSpeed);
// 		  		});
// 		  	});
// 	  	} else {
// 		  	//Modify Position for each floating container
// 		  	$('.ir-floating-container').each(function (e) {
// 		  		//Modify Each Floating item in the floating container.
// 		  		$(this).find('.ir-floating-item-mobile').each(function (e) {
// 		  			var topOffset = $(this).offset().top;
// 		  			var height = $(this).parent().height();
// 		  			var maxFloatDistance = floatDistance;
// 		  			// var maxFloatDistance = height * adjustmentCoefficient;
		  			
// 		  			var topDistance = topOffset - scrollDistanceToTop;
// 		  			var topDistancePercent = topDistance / height; // -200% to 200%
// 		  			topDistancePercent = ((topDistancePercent > maxHeightPercentage) ? maxHeightPercentage : topDistancePercent);
// 		  			topDistancePercent = ((topDistancePercent < -maxHeightPercentage) ? -maxHeightPercentage : topDistancePercent);
// 		  			topDistance = topDistancePercent * maxFloatDistance;
		  			
// 		  			$(this).stop().translate3d({ x: 0, y: topDistance, z: 0}, animationSpeed);
// 		  		});
// 		  	});
// 	  	}
//     }
    
//   	$(window).scroll(function (event) {
//   		updateParallaxItems(0);
// 	});
	
// 	updateParallaxItems(0);
// })(jQuery);