(function($) {
	
	$(document).ready(function() {
	
		var page_template = $('#page_template');
	   	var header_fields = $('#pods-meta-header-hero-carousel');
	    
	    	page_template.change(function() {
			    
			if ($(this).val() === 'template-about.php') {
				header_fields.show();
			} else if ($(this).val() === 'template-custom.php') {
				header_fields.hide();
			} else if ($(this).val() === 'something else') {
			} else {
				header_fields.show();
			}

		    }).change();

		});

})(jQuery); 
