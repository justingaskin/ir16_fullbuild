
// Script events for Waypoints and Parallax behavior

jQuery(document).ready(function($) {
	

	// GLOBALS
	$('.ir-button:hover').hover(function() {
		$(this).css('background-color','rgba(0, 0, 0, 0)');
		$(this).css('color','black');
	});

	// HREF GREEN BORDERING - link (as '<mark>' in posts) background behavior - add the mark as green line behind the element 
	// $('mark').each(function() {
	// 	var mark_bgWidth = $('mark').width();
		// $(this).append('<div class="cta_markbg" style="left:-'+mark_bgWidth+'px;width:'+mark_bgWidth+'px;"></div>');
	// });

	// Header full width assurance
	$('.ir-slick-carousel').css('opacity','1').css('width','100vw');

	// *** Header Carousel manipulation hacks
	// home video as first slide
	if ($('#page-id-7').length) {
		$('#slick-carousel-position-0').append('<div class="ir__homeVideo_header"><video autoplay="autoplay" muted="muted" loop="loop"><source src="app/themes/idearebel_16/assets/images/Home_HeaderVideo.mp4" /></div>');
		$('.work_clientLogos').hide();
	}
	// header image background css value instead of stupid overkill carousel
	// get the image we need
	var header_bgImg = $('.header-ir-image').attr('src');
	// put the image into the header block as background image and hide the bummercarousel.exe
	// TODO = get this sucker to call the appropriate file for the viewport dimensions
	$('[id^="slick-carousel-position"] picture img').each(function() {
		var header_bgImg2 = $(this).attr('src');
		$(this).closest('[id^="slick-carousel-position"]').css({
			'background-image': 'url("' + header_bgImg2 + '")',
			'background-size': 'cover',
			'background-position': 'center center',
			width: '100vw'			
		});
	});
	// hide the gd thing
	$(".header-ir-image-container picture").hide();
	// put the bg-image center top and put the only useful thing on the front z
	$('[id^="slick-carousel-position"]').css('height','100vh');
	$('.ir-slick-carousel').css('opacity','1').css('width','100vw');

	// single post header image - do that thing above that moves images and waves things around to make it like yeah cool hey lookie here, but for single posts
	if($('.single-post').length) {
		$('[id^="single_headerImg"] img').each(function() {
			var single_header_bgImg = $('[id^="single_headerImg"] img').attr('src');
			// console.log(single_header_bgImg);
			$('.header-ir-image-full').css({
				'background-image': 'url("' + single_header_bgImg + '")',
				'background-size': 'cover',
				'background-position': 'center center'
			});
		});
	}

	// *** Single Page hacks

	// work page mobile option to use .ir-work-section picture as css background for the .section instead of a floating element vying for position with the title - aka, how i spent my time wrestling with 3000 different css methods
	if($('#page-id-9').length) {
		$('[class^="ir-work-section"] img').each(function() {
			var single_workSection_bgImg = $(this).attr('src');
			console.log(single_workSection_bgImg);
			$(this).closest('.ir-work-section').css({
				'background-image': 'url("' + single_workSection_bgImg + '")',
				'background-size': 'cover',
				'background-position': 'center center'
			});
		});
		$('#page-id-9 .section').css('padding','0').each(function(i){
			$(this).addClass('col-md-6 col-sm-6 col-xs-12');
			$('.ir-work-section-titlebox').css('opacity','1');
			$('.ir-work-section-excerpt').hide();
		});
		$('#page-id-9 .cult_container').hide();
	}

	// superfluidity in this case is to move an element into where it should be whence you're holding a miniature device
	if ($('#page-id-190').length) {
		if ($(window).width() < 320) {
			var post_slugItmImg = $(".post_slug-item-image");
			$( ".ir-project-idea-container" ).prependTo(post_slugItmImg);
	// $(".post_slug-item-image").css('width','120%');
		}
	}

	// *** Home slider for Work projects
	if ($('#page-id-7').length) {
		// Home Waypoints for work project scroller
		var waypoint = new Waypoint({
			element: $('.home-cta'),
			handler: function() {
				// $('#home_workNav').css('left','20px');
				$('#home_workNav').animate({opacity: 1}, 2000);
				// TODO slide the menu out to cue the user for UX
			}
		})

		// Home Waypoint Menu Buttons
		// height of the entire work section including hidden by overflow
		var home_workBtnHeight = $('#home_workItems').prop('scrollHeight');
		$('#home_workItems .section').each(function(i){
			$(this).addClass('ir-work-section_' + (i+1));
		});

		// scroll to the work-section within the hidden overflow
		var nav1 = $('#home_workItems .ir-work-section_1');
		var nav2 = $('#home_workItems .ir-work-section_2');
		var nav3 = $('#home_workItems .ir-work-section_3');
		var nav4 = $('#home_workItems .ir-work-section_4');

		// todo: truncate to single function
		$('#nav1').click(function(){
			$(nav1).ScrollTo();
		});
		$('#nav2').click(function(){
			$(nav2).ScrollTo();
		});
		$('#nav3').click(function(){
			$(nav3).ScrollTo();
		});
		$('#nav4').click(function(){
			$(nav4).ScrollTo();
		});
		// push slide images to background css to act as pseudo masking to ensure images fit better rather than workign with img elements that fight my will to conquer all
		$('.ir-work-section').each(function() {
		var home_workSlide = $(this).find('picture img').attr('src');
		$(this).find('picture').css({
			'background-image': 'url("' + home_workSlide + '")',
			'background-size': 'cover',
			'background-position': 'center center'
		});
		$(this).find('picture img').hide();
	});
	}

	// Lab Single Post
	// ** todo: move the header title into the full screen header image as title with date
	if ($('.ir-main .blogPost').length) {
		var blogTitle = $('.ir-main .blogPost .entry-title');
		var blogDate = $('.ir-main .blogPost .labPost_date');
		$(blogTitle).appendTo('#slick-carousel-position-0 .header-ir-titlebox h1');
		$(blogDate).appendTo('#slick-carousel-position-0 .header-ir-subtitle');

		console.log(blogTitle);
	}
	// *** todo: wrap indented images from post to 50% outside of left or right text body margins

});