(function($) {
  //==========
  //HEADER AND NAVIGATION NavBar..
  var keys = {37: 1, 38: 1, 39: 1, 40: 1};

	function preventDefault(e) {
	  e = e || window.event;
	  if (e.preventDefault) {
	      e.preventDefault();
	  }
	  e.returnValue = false;  
	}
	
	function preventDefaultForScrollKeys(e) {
	    if (keys[e.keyCode]) {
	        preventDefault(e);
	        return false;
	    }
	}
	
	function disableScroll() {
	  if (window.addEventListener) { // older FF
	      window.addEventListener('DOMMouseScroll', preventDefault, false);
	  }
	  window.onwheel = preventDefault; // modern standard
	  window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
	  window.ontouchmove  = preventDefault; // mobile
	  document.onkeydown  = preventDefaultForScrollKeys;
	}
	
	function enableScroll() {
	    if (window.removeEventListener) {
	        window.removeEventListener('DOMMouseScroll', preventDefault, false);
	    }
	    window.onmousewheel = document.onmousewheel = null; 
	    window.onwheel = null; 
	    window.ontouchmove = null;  
	    document.onkeydown = null;  
	}

	//Re-Center Items
	//Dynamic Position of Work TitleBox.
    function centerNavAbsoluteInParent(parentClass, centerClass) {
		$(parentClass).each(function() {
			var self = $(this);
			var titleBox = self.find(centerClass);
			titleBox.css("left", "calc(50% - " +  (titleBox.outerWidth(true) / 2.0) + "px)");
			titleBox.css("top", "calc(50% - " +  (titleBox.outerHeight(true) / 2.0) + "px)");
		});
  	}
	//nav-ir-logo-black
	//nav-ir-logo-white
  	var default_nav_black = false;
  	$('#nav-toggle-button').click(function() {
		var logoHasWhiteClass = $(this).hasClass('nav-ir-button-white');
		var logoSrc;
		//Close Menu
  		if($('#nav-ir-primary').hasClass('nav-primary-active')) {
	  		$('#nav-ir-primary').removeClass('nav-primary-active');
	  		$('#nav-ir-menu').removeClass('nav-menu-active');
	  		enableScroll();
	  		if(default_nav_black) {
	  			$(this).addClass('nav-ir-button-black');
	  			$(this).removeClass('nav-ir-button-white');
	  			logoSrc = $('.nav-ir-logo-img').attr('src');
	  			logoSrc = logoSrc.replace('white.png', 'black.png');
	  			$('.nav-ir-logo-img').attr('src', logoSrc);
	  		}
			$('.nav-menu-bar-line').each(function() {
				$(this).removeClass('x-mode');
			});
	  		//ANIMATION FADE OUT
	  		
    		$('#nav-ir-menu').fadeOut(200, function(e) {
    			$('#nav-ir-menu').hide();
    		});
    		$('#nav-background').fadeOut(200, function(e) {
    			$('#nav-background').hide();
    		});
	  	//Open Menu
	  	} else {
    		
	  		$('#nav-ir-primary').addClass('nav-primary-active');
	  		$('#nav-ir-menu').addClass('nav-menu-active');
	  		disableScroll();
	  		default_nav_black = $(this).hasClass('nav-ir-button-black');
	  		if(default_nav_black) {
	  			$(this).removeClass('nav-ir-button-black');
	  			$(this).addClass('nav-ir-button-white');
	  		}
  			logoSrc = $('.nav-ir-logo-img').attr('src');
  			logoSrc = logoSrc.replace('black.png', 'white.png');
  			$('.nav-ir-logo-img').attr('src', logoSrc);
			$('.nav-menu-bar-line').each(function() {
				$(this).addClass('x-mode');
			});
			
			
	  		//ANIMATION FADE IN
    		$('#nav-ir-menu').fadeIn(200);
    		$('#nav-background').fadeIn(200);
	  	}
	  	return false;
  	});
  
  	//Slick Carousel
  	console.log('init slick');
  	$('.ir-slick-carousel').on('init', function(slick) {
  	console.log('start slick');
        $('.ir-slick-carousel').animate({
        	opacity:1
        }, 1000, function() {
  		
  	console.log('done slick');
        	$('.ir-slick-carousel').css('opacity', 1);
        });
    }).slick();
    
  	function updatePageContent() {
	  	var active_id = $('.slick-active').attr('id');
	  	var active_id_split = active_id.split('-');
	  	var carousel_position = active_id_split[3];
	  	
		$('.active-carousel').addClass('inactive-carousel');
		$('.active-carousel').removeClass('active-carousel');
		$('#ir-carousel-' + carousel_position).addClass('active-carousel');
		$('#ir-carousel-' + carousel_position).removeClass('inactive-carousel');
  	}
  
  	function updateNavigationColor(past_header) {
	  	past_header = ((past_header === undefined) ? false : past_header);
	  	var logoSrc;
		var logoHasWhiteClass = $('#nav-toggle-button').hasClass('nav-ir-button-white');
	  	if(past_header || $('.slick-active').hasClass("append-nav-black")) {
  			$('#nav-toggle-button').addClass('nav-ir-button-black');
  			$('#nav-toggle-button').removeClass('nav-ir-button-white');
  			
  			logoSrc = $('.nav-ir-logo-img').attr('src');
  			logoSrc = logoSrc.replace('white.png', 'black.png');
  			$('.nav-ir-logo-img').attr('src', logoSrc);
	  	} else if($('.slick-active').hasClass("append-nav-white")) {
  			$('#nav-toggle-button').removeClass('nav-ir-button-black');
  			$('#nav-toggle-button').addClass('nav-ir-button-white');
  			logoSrc = $('.nav-ir-logo-img').attr('src');
  			logoSrc = logoSrc.replace('black.png', 'white.png');
  			$('.nav-ir-logo-img').attr('src', logoSrc);
	  	}
  	}
  
  	updateNavigationColor();
  
    function checkVideoHeaderPlay(isOldHeader) {
		var header_jPlayer = $('.slick-active')
		.find('.jp-container')
		.find('.jp-video')
		.find('.jp-type-single')
		.find('.jp-jplayer');
		
		if(header_jPlayer !== undefined) {
			header_jPlayer.jPlayer((isOldHeader) ? 'pause' : 'play');
		}
    }
  	var original_nav_black = $('.nav-ir-button').hasClass('nav-ir-button-black');
  	function slickMovePrevious()  {
  		checkVideoHeaderPlay(true);
	  	$('.ir-slick-carousel').slick('slickPrev');
	  	updatePageContent();
	  	updateNavigationColor();
	    original_nav_black = $('.nav-ir-button').hasClass('nav-ir-button-black');
  		checkVideoHeaderPlay();
  	}
  	
  	function slickMoveNext()  {
  		checkVideoHeaderPlay(true);
		$('.ir-slick-carousel').slick('slickNext');
	  	updatePageContent();
	  	updateNavigationColor();
	    original_nav_black = $('.nav-ir-button').hasClass('nav-ir-button-black');
  		checkVideoHeaderPlay();
  	}
  	
  	//Handle clicking event of the carousel
  	$('.header-ir-image-container').click(function(e, passEvent) {
  		if($('#nav-ir-primary').hasClass('nav-primary-active')) {
  			return true;
  		}
	  	//Click Event is on left 50% - Previous
	  	if(e.pageX < ($('.header-ir-image-container').width() / 2) || (passEvent !== undefined && passEvent.pageX < ($('.header-ir-image-container').width() / 2))) {
	  		slickMovePrevious();
	  	} else { //Click event is on right 50% - Next
	  		slickMoveNext();
	  	}
	  	return true;
  	});

  	$('.nav-ir-logo').click(function(e) {
  		window.location = $(this).attr('href');
  		return false;
  	});
  	
  	//Slick Resizing
  	$('.ir-slick-carousel').on('setPosition', function(event, slick, direction){
	  	  centerNavAbsoluteInParent('.header-ir-image-container', ".header-ir-titlebox");
	  	  var thisHeight = $(window).height();
	  	  var thisWidth = $(window).width();
	  	  $('.header-ir-image-full').css('height', thisHeight + "px");
	  	  $('.header-ir-image').each(function() {
	  	  	var aspectRatio = $(this).width() / $(this).height();
	  	  	
	  	  	var sliderContainer = $(this).parent().parent();
	  	  	if(sliderContainer.hasClass('header-ir-center')) {
	  	  		sliderContainer.css("height", thisHeight +"px");
		  	  	if(thisWidth >= (thisHeight * aspectRatio)) {
			  	  	$(this).css("width", "auto");
			  	  	$(this).css("height", thisHeight + "px"); 
			  	  	$(this).css("top", 0);
		  	  	} else {
			  	  	$(this).css("width", thisWidth + "px");
			  	  	$(this).css("height", "auto"); 
			  	  	$(this).css("top", ((thisHeight - (thisWidth / aspectRatio)) / 2) + "px");
		  	  	}
	  	  	} else {
	  	  		sliderContainer.css("max-height", thisHeight +"px");
		  	  	if(thisWidth >= (thisHeight * aspectRatio)) {
			  	  	$(this).css("width", "100%"); 
			  	  	$(this).css("height", "100%"); 
		  	  	} else {
			  	  	$(this).css("width", "auto");
			  	  	$(this).css("height", thisHeight + "px"); 
		  	  	}
	  	  	}
  	  	});
  	  
	  	//Gonna get confusing
	  	var videoContainer = $('.jp_container_header-video');
	  	if(videoContainer !== undefined) {
		  	var videoPlayerContainer = videoContainer.find('.jp-type-single').find('.jp-jplayer');
		  	var videoPlayer = videoPlayerContainer.find('video');
		  	if(videoPlayerContainer.width() > videoPlayer.width()) {
		  	  	videoPlayer.css("width", $(window).width() + "px"); 
		  	  	videoPlayer.css("height", "auto"); 
		  	} else if(videoContainer.height() > videoPlayer.height()) {
		  	  	videoPlayer.css("width", "auto"); 
		  	  	videoPlayer.css("height", thisHeight + "px");
		  	}
		  	//Align Center Vertical
	  		var difVertical = (videoPlayer.height() - videoContainer.height()) / 2;
	  		difVertical = (difVertical > 0) ? difVertical : 0;
	  		videoPlayer.css("margin-top", -difVertical + "px");
	  		//Align Center Horizontal
	  		var difHorizontal = (videoPlayer.width() - videoPlayerContainer.width()) / 2;
	  		difHorizontal = (difHorizontal > 0) ? difHorizontal : 0;
	  		videoPlayer.css("margin-left", -difHorizontal + "px");
		  	$('.jp_container_header-video').css("height", thisHeight + "px");
	  	}
	});
	
	//Slick Resize isn't triggered on Vertical move. Added this method to add horizontal resizing.
	var widthResizeValue = $(window).width(); 
	$(window).resize(function() {
	    if ($(window).width() === widthResizeValue) {
	    	$('.ir-slick-carousel').trigger('setPosition');	
	    }
	    widthResizeValue = $(window).width();
	});
	
  	$('.header-ir-select-button-container').click(function(e) {
  		e.stopPropagation();
  	});

	//Add on scroll method
  	var scrollWasBelowHeader = (scroll > $('header').height());
  	$(window).scroll(function (event) {
	  	var scrollHeight = $(window).scrollTop();
	  	var headerHeight = $('header').height();
	  	if(!scrollWasBelowHeader && (scrollHeight > headerHeight)) {
	  		scrollWasBelowHeader = true;
	  		updateNavigationColor(true);
	  	} else if(scrollWasBelowHeader && (scrollHeight <= headerHeight)){
	  		scrollWasBelowHeader = false;
	  		updateNavigationColor(original_nav_black);
	  	}
  	});
  	
  	//Setup on hover for header.
  	$('a').mouseover(
	  function() {
	  	// $(this).find('.ir-nav-h2').find('.ir-nav-menu-item').addClass('ir-nav-menu-item-hover');
	  	$(this).find('.ir-hover-span-underline').addClass('ir-hover-span-underline-active');
	  }).mouseout(function() {
	  	// $(this).find('.ir-nav-h2').find('.ir-nav-menu-item').removeClass('ir-nav-menu-item-hover');
	  	$(this).find('.ir-hover-span-underline').removeClass('ir-hover-span-underline-active');
	  }
	);
   
   //Cursor Icon Handler 
  	$('.ir-home-carousel').mousemove(
    	function(e) {
   			var parentOffset = $(this).offset(); 
    		var xVal = (e.pageX - parentOffset.left) / $(this).width();
    		var showClass = 'ir-home-cursor-' + ((original_nav_black) ? 'black' : 'white') + '' + ((xVal > 0.5) ? '-right' : '-left');
    		
    		if(!$(this).hasClass(showClass)) {
	    		$(this).removeClass('ir-home-cursor-black-left');
	    		$(this).removeClass('ir-home-cursor-black-right');
	    		$(this).removeClass('ir-home-cursor-white-left');
	    		$(this).removeClass('ir-home-cursor-white-right');
	    		$(this).addClass(showClass);
    		}
    	}
  	);
  	
  	//Handle nav scroll smooth scroll on nav click
  	$('.nav-scrolldown-button').click(function() {
  		var scrollButtonId = $(this).attr('id');
  		var splitId = scrollButtonId.split('-');
  		scrollButtonId = splitId[1];
  		
  		var target = $("#page-id-" + scrollButtonId);

  		$('html, body').stop().animate({
  			scrollTop: target.offset().top
  		}, 500);
  	});
  	
  	//Home Page Nav Switching Via project buttons.
  	$('.ir-home-nav-next').click(function() {
  		$("html, body").stop().animate({scrollTop:0}, '500', 'swing', function() {  
  			slickMoveNext();
		});
  		return false;
  	});
  	$('.ir-home-nav-prev').click(function() {
  		$("html, body").stop().animate({scrollTop:0}, '500', 'swing', function() {  
  			slickMovePrevious();
		});
  		return false;
  	});
  	
    $(this).scrollTop(0);
})(jQuery);
