(function($) {

    // Watch for lab post hover to fade in background image
    $(".labPost_link").on({
        mouseenter: function () {
            $(this).addClass('_is_on');
        },
        mouseleave: function () {
            $(this).removeClass('_is_on');
        }
    });

    // Apply Isotope to lab posts
    var grid = $('.isotopeGrid').isotope({
        // options...
        itemSelector: '.grid-item'
    });

    // Filter functions
    var filterFns = {
        // show if number is greater than 50
        numberGreaterThan50: function() {
            var number = $(this).find('.number').text();
            return parseInt(number, 10) > 50;
        }
    };

    // Bind filter button click
    $('.filters-button-group').on('click', 'button', function() {
        var filterValue = $(this).attr('data-filter');
        // use filterFn if matches value
        filterValue = filterFns[filterValue] || filterValue;
        grid.isotope({ filter: filterValue });
    });

    // Change is-checked class on buttons
    $('.button-group').each(function(i, buttonGroup) {
        var $buttonGroup = $(buttonGroup);
        $buttonGroup.on('click', 'button', function() {
            $buttonGroup.find('._is_active').removeClass('_is_active');
            $(this).addClass('_is_active');
        });
    });

})(jQuery);
