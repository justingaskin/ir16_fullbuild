(function($) {
	
	//Setup VR on hover mouse over.
  	$('.ir-vr-hover-swap-container').mousemove(
    	function(e) {
   			var parentOffset = $(this).offset(); 
    		var xVal = (((e.pageX - parentOffset.left) / $(this).width()) - 0.5) * 2;
    		var yVal = (((e.pageY - parentOffset.top) / $(this).height()) - 0.5) * 2;
    		var xValAbs = Math.abs(xVal);
    		var yValAbs = Math.abs(yVal);
    		var centerThreshold = 0.2;
    		var displayDirection;
    		var displayPosition;
    		var horizontalCount = 3;
    		var verticalCount = 2;
    		var horizontalThreshold = (1 / ( horizontalCount + 1));
    		var verticalThreshold = (1 / ( verticalCount + 1));
    		
    		//Is inside center
    		if(horizontalThreshold > xValAbs && verticalThreshold > yValAbs) {
    			//Show Center
    			displayDirection = 'center';
    			displayPosition = '';
    		//Vertical is higher priority
    		} else if(xValAbs > yValAbs) {
    			//Show Bottom
    			if(xVal > 0) {
    				displayDirection = 'right-'; 
    			//Show Top
    			} else {
    				displayDirection = 'left-';
    			}
    			displayPosition = Math.floor(xValAbs / horizontalThreshold);
    			displayPosition = ((displayPosition < 1) ? 1 : displayPosition);
    			displayPosition = ((displayPosition > horizontalCount) ? horizontalCount : displayPosition);
    		//Horizontal is higher priority
    		} else {
    			//Show Right
    			if(yVal < 0) {
    				displayDirection = 'down-';
    			//Show Left
    			} else {
    				displayDirection = 'up-';
    			}
    			displayPosition = Math.floor(yValAbs / verticalThreshold);
    			displayPosition = ((displayPosition < 1) ? 1 : displayPosition);
    			displayPosition = ((displayPosition > verticalCount) ? verticalCount : displayPosition);
    		}
    		
    		$('.ir-vr-hover-img').each(function() {
    			$(this).hide();
    		});
    		$('.ir-vr-hover-' + displayDirection + displayPosition).show();
    	}
  	);
  	
  	//Setup Converse Hover on Images
  	$('.ir-converse-shoe-inner').mouseover(
	  function() {
	  	$(this).find('.ir-converse-content').removeClass('smoothFadeOut');
	  	$(this).find('.ir-converse-content').addClass('smoothFadeIn');
	  }).mouseout(function() {
	  	$(this).find('.ir-converse-content').removeClass('smoothFadeIn');
	  	$(this).find('.ir-converse-content').addClass('smoothFadeOut');
	  }
	);
})(jQuery);