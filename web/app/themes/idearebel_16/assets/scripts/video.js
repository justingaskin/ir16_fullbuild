(function($) {
    $(document).ready(function(){
	  $('.jp-jplayer').each(function() {
	  	  var cssAncestor = $(this).parent().parent();
		  var width = cssAncestor.attr('max_width');
		  var height = cssAncestor.attr('max_height');
		  var is_header_video = cssAncestor.attr('is_header') !== undefined;
  	  	  	  
     	  if(width === undefined) {
     	  	width = '100%';
     	  }
     	  if(height === undefined) {
     	  	height = 'auto';
     	  }
	  	  $(this).jPlayer({
	  	  	ready: function () {
	      	  var video_url = cssAncestor.attr('video_url');
			  var thumb_url = cssAncestor.attr('thumb_url');
  	  	  	  if(is_header_video && cssAncestor.attr('front_page') === undefined) {
		          $(this).jPlayer("setMedia", {
		            m4v: video_url,
		            poster: thumb_url
		          }).jPlayer('play');
  	  	  	  } else {
		          $(this).jPlayer("setMedia", {
		            m4v: video_url,
		            poster: thumb_url
		          });
  	  	  	  }
	        },
	        cssSelectorAncestor: "#" + cssAncestor.attr('id'),
        	swfPath: "/js",
	        supplied: "m4v",
	        loop: is_header_video,
	        size: {
	        	width: width,
	        	height: height
	        }
	      });
	    
	  	$(this).bind($.jPlayer.event.play, function(e) { 
	  		$(this).parent().find('.jp-gui').hide();
	  		$(this).parent().find('.jp-gui-pause').hide();
	  		return true;
		});
	      
	  	$(this).bind($.jPlayer.event.ended, function(e) {
	  		$(this).parent().find('.jp-gui').show();
	  		$(this).parent().find('.jp-gui').find('.jp-gui-play').find('.jp-video-play').show();
	  		$(this).parent().find('.jp-gui-pause').hide();
	  		return true;
		});
		
		$(this).click(function() {
			if($(this).data().jPlayer.status.paused) {
				$(this).jPlayer("play");
			} else {
				$(this).jPlayer("pause");
			}
		});
	  });
	  
	  $(".jp-jplayer").mouseover(
	    function() {
	    	if(!$(this).parent().parent().hasClass('jp_container_header-video')) {
  	  	  		$(this).find('video').prop('controls', true);
  	  	 	}
	    }).mouseout(function() {
  		  $(this).find('video').removeAttr('controls');
	    }
	  );
	  
	  $('.jp_container_header-video').find('.jp-type-single').find('.jp-jplayer').find('video').click(function(e) {
  		$('.header-ir-image-container').trigger('click', [e]);
  		e.stopPropagation();
  		return false;
	  });
    });
})(jQuery);