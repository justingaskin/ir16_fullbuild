(function($) {
	
	//Add hover display for projects.
	$("a.ir-work-section-link").mouseover(
	  function() {
	  	$(this).find('.ir_workTitle').removeClass('smoothFadeOut');
	  	$(this).find('.ir_workTitle').addClass('smoothFadeIn');
	  }).mouseout(function() {
	  	$(this).find('.ir_workTitle').removeClass('smoothFadeIn');
	  	$(this).find('.ir_workTitle').addClass('smoothFadeOut');
	  }
	);

	$('.ir-work-section').each(function(){
		var self = $(this).find(".ir_workTitle").height();
		var irwst = $(this).find(".ir_workTitle");
		$(irwst).css("top", "calc(50% -" + (self / 2) + "px)");
	});

  	//Dynamic Position of Work TitleBox.
    	function centerAbsoluteInParent(parentClass, centerClass) {
		$('.ir-work-section').each(function() {
			var self = $(this);
			var titleBox = self.find(".ir_workTitle");
			var titleBoxW = self.find(".ir_workTitle").width();
			// titleBox.css("left", "calc(50% - " + (titleBoxW / 2.0) + "px)");
			titleBox.css("top", "calc(50% - 80px / 2)");
			// console.log(titleBoxW);
		});
  	}
  	
	centerAbsoluteInParent('.ir-work-section', '.ir_workTitle');
	
	$(window).resize(function() {
		centerAbsoluteInParent('.ir-work-section', '.ir_workTitle');
	});
	 
})(jQuery);