<?php
/**
 * Template Name: Media Kit Template
 */
?>

<div id="ir__mediaKit">
	<div class="ir__mk_intro">
		<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<?php if (get_post_type() === 'post') { get_template_part('templates/entry-meta'); } ?>
	  <div class="entry-summary">
		<?php the_excerpt(); ?>
	  </div>
	</div>
	<div class="ir__mk_logos">
		<div class="ir__mk_logosTitle">LOGOS</div>
		<div class="ir__mk_logosAssets">
			<img class="ir__mk_logosIMG" src="ir_LogoIMG_textBlack.png" />
			<br>
			<span class="ir__mk_logosAssetsCopy">
				Full Logo (black)
			</span>
			<div class="ir__mk_logosBtns">
				<a href="ir_LogoIMG_textBlack.png"><span class="ir__mk_logoBTN">DOWNLOAD .EPS</span></a>
				<a href="ir_LogoIMG_textBlack.png"><span class="ir__mk_logoBTN">DOWNLOAD .PNG</span></a>
			</div>
			<img class="ir__mk_logosIMG" src="ir_LogoIMG_textWhite.png" />
			<br>
			<span class="ir__mk_logosAssetsCopy">
				Full Logo (white)
			</span>
			<div class="ir__mk_logosBtns">
				<a href="ir_LogoIMG_textBlack.png"><span class="ir__mk_logoBTN">DOWNLOAD .EPS</span></a>
				<a href="ir_LogoIMG_textBlack.png"><span class="ir__mk_logoBTN">DOWNLOAD .PNG</span></a>
			</div>
			<img class="ir__mk_logosIMG" src="ir_LogoIMG_squareBlack.png" />
			<br>
			<span class="ir__mk_logosAssetsCopy">
				Full Logo (black)
			</span>
			<div class="ir__mk_logosBtns">
				<a href="ir_LogoIMG_textBlack.png"><span class="ir__mk_logoBTN">DOWNLOAD .EPS</span></a>
				<a href="ir_LogoIMG_textBlack.png"><span class="ir__mk_logoBTN">DOWNLOAD .PNG</span></a>
			</div>
			<img class="ir__mk_logosIMG" src="ir_LogoIMG_squareWhite.png" />
			<br>
			<span class="ir__mk_logosAssetsCopy">
				Full Logo (white)
			</span>
			<div class="ir__mk_logosBtns">
				<a href="ir_LogoIMG_textBlack.png"><span class="ir__mk_logoBTN">DOWNLOAD .EPS</span></a>
				<a href="ir_LogoIMG_textBlack.png"><span class="ir__mk_logoBTN">DOWNLOAD .PNG</span></a>
			</div>
		</div>
	</div>
	<div class="ir__mk_rebels">		
		<div class="ir__mk_leadersTitle">Leadership</div>
		<div class="ir__mk_leaders">
			<img class="ir__mk_LDRheadshot" />
			<span class="ir__mk_LDRname">Jamie Garratt</span>
			<span class="ir__mk_LDRtitle">CEO & Founder</span>
			<div class="ir__mk_LDRblurb">Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.</div>
			<a class="ir__mk_LDRimg" href="#"><span>headshot link</span></a>
		</div>
	</div>
</div>
