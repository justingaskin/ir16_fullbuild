<footer class="content-info">
  <div class="container mainFooter">
  	<!-- Phone Number -->
  	<div class="footer-call row">
  		<?php Roots\Sage\Extras\display_rand_call_prompt(); ?>
  	</div>
  	<!-- Offices -->
  	<div class="footer-offices row">
  		<div class="footer-offices-inner">
	  		<!-- TODO: Setup Weather Feed for each Office -->
	  		<?php Roots\Sage\Extras\display_offices_footer(); ?>
  		</div>
  	</div>
  	<!-- IR -->
  	<div class="footer-ir-label row">
  		<a href="<?php echo home_url(); ?>"><img class="footer-ir-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/footer-ir-logo.png" alt="Idea Rebel" /></a>
  	</div>
  	<!-- Social Icons -->
  	<div class="footer-social-icon-container row">
		<div id="footer-instagram" class="footer-social-icon col-xs-3">
			<a class="footer-social-icon-inner" href="https://www.instagram.com/idearebel">
				<i class="fa fa-instagram"></i>
			</a>
		</div>
		<div id="footer-twitter" class="footer-social-icon col-xs-3">
			<a class="footer-social-icon-inner" href="https://twitter.com/idearebel">
				<i class="fa fa-twitter"></i>
			</a>
		</div>
		<div id="footer-facebook" class="footer-social-icon col-xs-3">
			<a class="footer-social-icon-inner" href="https://www.facebook.com/idearebel/">
				<i class="fa fa-facebook"></i>
			</a>
		</div>
		<div id="footer-linkedin" class="footer-social-icon col-xs-3">
			<a class="footer-social-icon-inner" href="https://www.linkedin.com/company/idea-rebel">
				<i class="fa fa-linkedin"></i>
			</a>
		</div>
  	</div>
  </div>
</footer>
<?php wp_footer(); ?> 

