<section id="servicesTimeline" class="servicesTimeline ir-content-padding  col-xs-12">
	<div class="timelineBackground timelineBackground--top">
		<?php echo __('OUR PROCESS'); ?></div>
		<div class="timelineBackground-startTime"></div>
		<div class="timelineBackground-startTime__lineTop"></div>
</section>

<section id="services_timeItems">
<?php get_template_part('templates/content', 'services-timeline-item'); ?>
</section>