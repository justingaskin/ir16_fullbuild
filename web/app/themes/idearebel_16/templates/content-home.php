<?php the_content(); ?>
<div id="home_master"> 
	<div id="home_workNav">	
		<a id="nav1" class="workNav_notch"></a>
		<a id="nav2" class="workNav_notch"></a>
		<a id="nav3" class="workNav_notch"></a>
		<!-- <a id="nav4" class="workNav_notch"></a> -->
	</div>
	<div id="fullpage">
		<div id="home_workItems">
			<?php Roots\Sage\Extras\display_work_projects_onHome(); ?>
		</div>
	</div>

	<div id="home_cultureMaster">
		<?php
			$args = array('post_type' => 'home_culture',
						    'posts_per_page' => '1',
							'orderby' => 'rand');
			$homeCulture_query = new \WP_Query($args);
			while($homeCulture_query->have_posts()) {
				$homeCulture_query->the_post();
				echo '<div class="cult_container">';
					echo '<div class="cult_bg"></div>';
					echo '<div class="cult_img">';
					echo the_post_thumbnail();
				// echo pods_image( get_post_meta(get_the_ID(), 'culture_image_desktop', true ), 'original');
				echo '</div>';
				echo '<div class="cult_copy"><div class="cult_title"><h3>'.get_the_title().'</h3></div>';
				echo '<div class="cult_content">';
				the_content();
				echo '</div></div></div>';
			}	
			wp_reset_postdata();
		?>
	</div>
	<div id="home_postsRow" class="row">
		<div class="home_postsRow1 col-xs-12 col-sm-4 col-md-3 col-lg-3">
			<h4>Latest</h4>
			<div class="home_postsLoop">
				<?php
					$args = array('cat' => '243',
								    'posts_per_page' => '1');
					$homeNews_query = new \WP_Query($args);
					while($homeNews_query->have_posts()) {
						$homeNews_query->the_post();
						echo '<div class="homeNews_title"><h3><a href="'.get_permalink().'">'.get_the_title().'</a></h3></div>';
						echo '<div class="homeNews_date">'.get_the_date().'</div>';
					}	
					wp_reset_postdata();
				?>
			</div>
		</div>
		<div class="home_postsRow2 col-xs-12 col-sm-4 col-md-3 col-lg-3">
			<h4>Latest</h4>
			<div class="home_postsLoop">
				<?php
					$args = array('cat' => '243',
								    'posts_per_page' => '1',
								    'offset' => 1);
					$homeNews_query = new \WP_Query($args);
					while($homeNews_query->have_posts()) {
						$homeNews_query->the_post();
						echo '<div class="homeNews_title"><h3><a href="'.get_permalink().'">'.get_the_title().'</a></h3></div>';
						echo '<div class="homeNews_date">'.get_the_date().'</div>';
					}	
					wp_reset_postdata();
				?>
			</div>
		</div>
		<div class="home_postsRow3 col-xs-12 col-sm-4 col-md-3 col-lg-3">
			<h4>Latest</h4>
			<div class="home_postsLoop">
				<?php
					$args = array('cat' => '243',
								    'posts_per_page' => '1',
								    'offset' => 2);
					$homeNews_query = new \WP_Query($args);
					while($homeNews_query->have_posts()) {
						$homeNews_query->the_post();
						echo '<div class="homeNews_title"><h3><a href="'.get_permalink().'">'.get_the_title().'</a></h3></div>';
						echo '<div class="homeNews_date">'.get_the_date().'</div>';
					}	
					wp_reset_postdata();
				?>
			</div>
		</div>
		<div class="home_postsRow4 col-xs-12 col-sm-4 col-md-3 col-lg-3">
			<h4>Ideas</h4>
			<div class="home_postsLoop">
				<?php
					$args = array('category_name' => 'ideas',
								    'posts_per_page' => '1');
					$homeNews_query = new \WP_Query($args);
					while($homeNews_query->have_posts()) {
						$homeNews_query->the_post();
						echo '<div class="homeNews_title"><h3><a href="'.get_permalink().'">'.get_the_title().'</a></h3></div>';
						echo '<div class="homeNews_date">'.get_the_date().'</div>';
					}	
					wp_reset_postdata();
				?>
			</div>
		</div>
	</div>
	<div class="work_clientLogos">
		<?php echo do_shortcode('[select-clients]'); ?>
	</div>
</div>