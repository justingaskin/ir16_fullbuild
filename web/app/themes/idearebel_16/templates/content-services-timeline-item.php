<?php

//grab all the services
$params = array(
        'limit' => -1,
		'orderby' => 'display_order ASC'
    );

$pod = pods('service', $params);

$services = array();

$pod_num = 0;

while ( $pod->fetch() ) {

  $services[$pod_num] = array();

  $services[$pod_num]['icon'] = $pod->field('service_icon');
  $services[$pod_num]['name'] = $pod->field('service_category');
  $services[$pod_num]['content'] = $pod->field('service_content');
  $services[$pod_num]['keywords_label'] = $pod->field('service_keywords_label');
  $services[$pod_num]['keywords'] = explode(PHP_EOL, $pod->field('service_keywords'));
  $services[$pod_num]['alignment'] = $pod->field('services_alignment');
  $services[$pod_num]['background_color'] = $pod->field('services_content_bg_color');

  $services[$pod_num]['first_image'] = array();
  $services[$pod_num]['first_image']['widescreen'] = $pod->field('services_image_1_widescreen');
  $services[$pod_num]['first_image']['desktop'] = $pod->field('services_image_1_desktop');
  $services[$pod_num]['first_image']['tablet'] = $pod->field('services_image_1_tablet');
  $services[$pod_num]['first_image']['mobile'] = $pod->field('services_image_1_mobile');

  $services[$pod_num]['second_image'] = array();
  $services[$pod_num]['second_image']['widescreen'] = $pod->field('services_image_2_widescreen');
  $services[$pod_num]['second_image']['desktop'] = $pod->field('services_image_2_desktop');
  $services[$pod_num]['second_image']['tablet'] = $pod->field('services_image_2_tablet');
  $services[$pod_num]['second_image']['mobile'] = $pod->field('services_image_2_mobile');


  if(($services[$pod_num]['second_image']['widescreen'] || $services[$pod_num]['second_image']['desktop'] || $services[$pod_num]['second_image']['tablet']  || $services[$pod_num]['second_image']['mobile']) == false){
  	$services[$pod_num]['second_image'] = false;
  }
  else{
  	$services[$pod_num]['second_image'] = Roots\Sage\Extras\add_responsive_image_element(array(
							"wide_url" => $services[$pod_num]['second_image']['widescreen']['guid'],
							"desk_url" => $services[$pod_num]['second_image']['desktop']['guid'],
							"tab_url" => $services[$pod_num]['second_image']['tablet']['guid'],
							"mob_url" => $services[$pod_num]['second_image']['mobile']['guid']
						), $services[$pod_num]['name'] . ' secondary image', 'serviceItem_secondImage');
  }

  $services[$pod_num]['first_image'] = Roots\Sage\Extras\add_responsive_image_element(array(
							"wide_url" => $services[$pod_num]['first_image']['widescreen']['guid'],
							"desk_url" => $services[$pod_num]['first_image']['desktop']['guid'],
							"tab_url" => $services[$pod_num]['first_image']['tablet']['guid'],
							"mob_url" => $services[$pod_num]['first_image']['mobile']['guid']
						), $services[$pod_num]['name'] . ' main image', 'serviceItem_firstImage');

  $pod_num++;
}

?>
<?php $index = 0; ?>
<?php $trig = 0; ?>
<?php $para = 0; ?>
<?php $anim = 0; ?>
<?php foreach($services as $service){ ?>
<div id="<?php echo "trig_$trig"; $trig++; ?>"></div>
<div id="<?php echo "para_$para"; $para++; ?>" class="">
<article id="<?php echo "anim_$anim"; $anim++; ?>" class="serviceItem parallaxParent <?php echo "service__item-$index"; $index++; ?> serviceItem_<?php print strtolower($service['name']); ?> serviceItem--<?php print strtolower($service['alignment']); ?> col-xs-12" data-aligment="<?php print strtolower($service['alignment']); ?>">
	<div id="service_primContAnim" class="serviceItem_primaryContainer col-xs-12">
		<div class="serviceItem_descriptionWrapper col-xs-12 col-sm-6">
			<img class="serviceItem_icon" src="<?php print $service['icon']['guid']; ?>">
			<h3 class="serviceItem_name"><?php print $service['name']; ?></h3>
			<p class="serviceItem_description"><?php print $service['content']; ?></p>
		</div>
		<div class="serviceItem_imageWrapper col-xs-12 col-sm-6">
			<?php print $service['first_image']; ?>
		</div>
		<div class="serviceItem_secondaryContainer <?php echo "service__keywords-$index"; $index++; ?> col-xs-12">
			<?php if($service['second_image']){?>
			<div class="serviceItem_secondaryImageWrapper col-xs-12 col-sm-6">
				<?php print $service['second_image']; ?>
			</div>
			<?php }?>
			<div class="serviceItem_listingsWrapper col-xs-12 col-sm-6">
				<h4 class="serviceItem_listingsName"><?php print $service['keywords_label']; ?></h4>
				<ul class="serviceItem_listings">
					<?php if(count($service['keywords']) > 0 ){?>
						<?php foreach($service['keywords'] as $keyword){
							print '<li class="serviceItem_listing">'. $keyword .'</li>';
						} ?>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>
</article>
</div>
<?php }?>