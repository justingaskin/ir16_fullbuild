<?php

//set the first office to load posting right away, we may use location based stuff later.
$default_office = 'van';

//grab all the offices
$params = array(
        'limit' => -1,
    );

$pod = pods('office', $params);

$offices = array();

$pod_num = 0;

while ( $pod->fetch() ) {
  $offices[$pod_num] = array();

  $offices[$pod_num]['city'] = $pod->field('city');
  $offices[$pod_num]['city_abrev'] = $pod->field('city_abrev');

  $pod_num++;
}
//get all the job postings and sort them by office.
$params = array(
        'limit' => -1,
    );

$pod = pods('jobpost', $params);

$jobs = array();

while ( $pod->fetch() ) {
  $job_location = $pod->field('job_location');
  $job_location = get_post_meta($job_location['ID'], 'city_abrev', true);

  if(!array_key_exists($job_location, $jobs)){
    $jobs[$job_location] = array();
  }

  $job = array();
  $job['job_title'] = $pod->field('job_title');
  $job['job_secondary_title'] = $pod->field('job_secondary_title');
  $job['job_location'] = $pod->field('job_location')['post_name'];
  $job['job_location_city_abbr'] = $job_location;
  $job['job_url'] = get_permalink($pod->row['ID']);

  $jobs[$job_location][] = $job;
}
?>
<section class="careersOfficePicker">
<?php foreach($offices as $key => $office){ ?>
  <a class="ir-btn ir-btn-office-picker button <?php if(strtolower($office['city_abrev'])== $default_office){print 'ir-btn-office-picker--active';} ?>" data-office="<?php print strtolower($office['city_abrev']); ?>" href="#<?php print strtolower($office['city_abrev']); ?>"><span class="officeName-abrev"><?php print $office['city_abrev']; ?></span>
<span class="officeName-full"><?php print $office['city']; ?></span>
  </a>
<?php }?>
</section>
<section class="careersListings">
<?php foreach($jobs as $office => $job){ ?>
  <ul class="jobListings jobListings-<?php print strtolower($office); if(strtolower($office) == $default_office){print ' jobListings--active';} ?>">
  <?php foreach($job as $key => $listing){?>
      <li class="jobListing job">
        <h3 class="jobTitle"><a class="jobListing-link" href="<?php print $listing['job_url']; ?>"><?php print $listing['job_title']; ?></a></h3>
        <div class="joblisting_brief">
          <i class="jobSecondaryTitle ir-italic"><?php print $listing['job_secondary_title']; ?></i>
        </div>
      </li>
  <?php }?>
  </ul>
<?php }?>
</section>