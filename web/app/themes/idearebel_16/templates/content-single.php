<?php while (have_posts()) : the_post(); ?>
<article class="blogPost col-md-8 col-md-offset-2">

        
    <header>
        <h1 class="entry-title"><?php the_title(); ?></h1>
        <span class="labPost_date"><?php echo get_the_date('M d, Y'); ?></span>
    </header>
   
    <div id="single_headerImg" style="display:none;">
        <?php the_post_thumbnail(); ?>
    </div>

    <div class="blogPost_content">
        <?php the_content(); ?>
    </div>

    <footer>

        <div class="buttonContainer buttonContainer--top">

            <div class="buttonGroup">

                <a class="buttonGroup_btn button button---solid" href="<?php the_permalink(17); ?>" title="<?php _e('Back to labs'); ?>"><span><?php _e('Back'); ?></span></a>
                <button class="buttonGroup_btn button button---solid"><span><?php _e('Share'); ?></span></button>

            </div>
            <!-- /.buttonGroup -->

        </div>
        <!-- /.buttonContainer -->    

    </footer>

</article>
<?php endwhile; ?>
