<header class="banner">
    <nav id="nav-ir-primary" class="nav-primary">
        <div id="nav-bar-container">
            <div class="nav-ir-logo-container">
                <a class="brand nav-ir-logo" href="<?= esc_url(home_url('/')); ?>"><img class="nav-ir-logo-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/nav-ir-logo-white.png" alt="IR" /></a>
            </div>
            <div class="nav-ir-button-container">
                <a id='nav-toggle-button' class='nav-ir-button'>
                    <div id="menu-box" class="boxes">
                        <div id="nav-bar-1" class="nav-menu-bar-line"></div>
                        <div id="nav-bar-2" class="nav-menu-bar-line"></div>
                        <div id="nav-bar-3" class="nav-menu-bar-line"></div>
                    </div>
                </a>
            </div>
        </div>
        <div id="nav-background" class="nav-background"></div>
        <!-- TODO: Update mobile nav to have white background below header, and show only on scroll up. -->
        <!-- TODO: Update images for Nav Down arrow.... -->
        <div id="nav-ir-menu" class="nav-menu-display">
            <?php  
		    if (has_nav_menu('primary_navigation')) :
		    	wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'link_before' => "<h2 class='ir-nav-h2'><span class='ir-nav-menu-item ir-hover-span-underline'>", 'link_after' => "</span></h2>"]);
		    endif;
		  	?>
                <div class="header-social-icon-container row">
                    <div id="header-instagram" class="footer-social-icon col-xs-3">
                        <a class="footer-social-icon-inner" href="https://www.instagram.com/idearebel">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </div>
                    <div id="header-twitter" class="footer-social-icon col-xs-3">
                        <a class="footer-social-icon-inner" href="https://twitter.com/idearebel">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </div>
                    <div id="header-facebook" class="footer-social-icon col-xs-3">
                        <a class="footer-social-icon-inner" href="https://www.facebook.com/idearebel/">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </div>
                    <div id="header-linkedin" class="footer-social-icon col-xs-3">
                        <a class="footer-social-icon-inner" href="https://www.linkedin.com/company/idea-rebel">
                            <i class="fa fa-linkedin"></i>
                        </a>
                    </div>
                </div>
        </div>
    </nav>
<div id='ir-header' class="header-ir-image-full <?php  echo ((is_front_page()) ? " ir-home-carousel " : " " );?>">
        <!-- CLASS OPTIONS: header-ir-image-full header-ir-image-half header-ir-image-none -->
       <div class="ir-slick-carousel" data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "arrows":false, "draggable": false, "variableHeight": true}'>
            <?php 
	  			Roots\Sage\Extras\display_ir_carousel(get_the_ID());
			?>
        </div>
    </div>
</header>