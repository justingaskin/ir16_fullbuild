    <div id='ir-header' class="header-ir-image-full <?php echo ((is_front_page()) ? " ir-home-carousel " : " " );?>">
        <!-- CLASS OPTIONS: header-ir-image-full header-ir-image-half header-ir-image-none -->
        <div class="ir-slick-carousel" data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "arrows":false, "draggable": false, "variableHeight": true}'>
            <?php 
	  			Roots\Sage\Extras\display_ir_carousel(get_the_ID());
			?>
        </div>
    </div>