<article <?php post_class(); ?>>
  <div class="ir-content entry-content container">
  	<?php the_content(); ?>
  	<?php get_template_part('templates/content', 'digital-icons'); ?>
  </div>
</article>

<div id="section__container">
	<?php get_template_part('templates/content', 'services-timeline'); ?>
</div>
<div id="timeline__sectionsEnd" class="timelineBackground timelineBackground--bottom">
	<div class="timelineBackground-startTime__lineEnd"></div>
	<div class="timelineBackground-endTime"></div>
</div>

<div class='services__seeWork'>
	<a class="ir-btn ir-btn-black ir-btn-services button--black button" href="<?php print get_permalink(9); ?>">
		<span><?php echo __('See Our Work');?></span>
	</a>
</div>