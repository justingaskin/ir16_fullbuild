<?php while (have_posts()) : the_post(); ?>
<article class="jobPost">
   
    <div id="single_headerImg" style="display:none;">
        <?php the_post_thumbnail(); ?>
    </div>

    <div class="jobPost_content">
    	<div class="jobPost_intro">
	        <?php $job_intro = get_post_meta( get_the_ID(), 'job_introduction', true ); ?>
	        <div class="job_intro"><strong><?php echo $job_intro; ?></strong></div>
	    </div>

	    <div class="jobPost_resp">
	    	<h3>RESPONSIBILITIES:</h3>
	        <?php $jobPost_resp = get_post_meta( get_the_ID(), 'job_responsibilities', true ); ?>
	        <div class="jobPost_resp"><strong><?php echo $jobPost_resp; ?></strong></div>
	    </div>

	    <div class="job_exp">
	    	<h3>EXPERIENCE:</h3>
	        <?php $job_exp = get_post_meta( get_the_ID(), 'job_experience', true ); ?>
	        <div class="job_exp"><strong><?php echo $job_exp; ?></strong></div>
	    </div>

	    <div class="jobPost_loc">
	    	<h3>LOCATION:</h3>
	        <?php $jobPost_loc = get_post_meta( get_the_ID(), 'job_location_title', true ); ?>
	        <div class="jobPost_loc"><strong><?php echo $jobPost_loc; ?></strong></div>
	    </div>

	    <div class="jobPost_em">
	        <?php $jobPost_em = get_post_meta( get_the_ID(), 'job_email_to_address', true ); ?>
	        <div class="jobPost_em"><strong>Apply via tweet @idearebel or email <?php echo $jobPost_em; ?></strong></div>
	    </div>
    </div>

</article>
<?php endwhile; ?>