<div class="row">
    <div class="col-sm-12">
        <div class="buttonContainer">
            <div class="buttonGroup filters-button-group">
                <button class="buttonGroup_btn button _is_active" data-filter="*"><span><?php _e('Ideas'); ?></span></button>
                <?php
                // Get list of categories ordered by Name
                $lab_categories = get_categories(array(
                    'orderby' => 'name',
                    'order'   => 'ASC'
                ));
                // Output each category link as a button filter for the posts
                foreach ($lab_categories as $lab_category):
                    echo '<button class="buttonGroup_btn button" data-filter=".' . $lab_category->slug . '"><span>' . $lab_category->name . '</span></button> ';
                endforeach;
                ?>
            </div>
            <!-- /.buttonGroup -->
        </div>
        <!-- /.buttonContainer -->
    </div>
    <!-- /.col-sm-12 -->
</div>
<!-- /.row -->
<div class="labPosts">
    <div class="isotopeGrid">
        <div class="row">
            <?php
            // WP_Query arguments
            $args = array (
                'post_status'  => array( 'publish' ),
                'order' => 'DESC',
                'orderby' => 'date',
                'cache_results' => true,
            );

            // The Query
            $lab_posts = new WP_Query( $args );

            // The Loop
            if ($lab_posts->have_posts()):

                while ($lab_posts->have_posts()):
                    $lab_posts->the_post(); 

                    // Get categories assigned to post
                    $post_categories = wp_get_post_categories(get_the_ID());
                    $cats = array();
                         
                    // Generate a n array ot category names and slugs
                    foreach ($post_categories as $category):
                        $cat = get_category($category);
                        $cats[] = array('name' => $cat->name, 'slug' => $cat->slug);
                    endforeach;
                    ?>

                    <div class="col-sm-12 col-md-6 col-lg-6 csstransitions grid-item<?php foreach ($cats as $cat): echo ' ' . $cat['slug']; endforeach; ?>">
                        <div class="labPost">
                                <a class="labPost_link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                    <div class="labPost_bg" style="background-image: url('http://lorempixel.com/700/700/');"></div>
                                    <div class="labPost__container">
                                        <div class="labPost_inner">
                                            <h3 class="labPost_title"><?php the_title(); ?></h3>
                                            <span class="labPost_date"><?php echo get_the_date('M d, Y'); ?></span>
                                        </div>
                                    </div>
                                </a>
                            <!-- /.labPost_link -->
                        </div>
                        <!-- /.labPost -->
                    </div>
                    <!-- /.col-lg-6 -->
                <?php endwhile;
            else:
                // no posts found
            endif;
            // Restore original Post Data
            wp_reset_postdata();
            ?>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.isotopeGrid -->
</div>
<!-- /.labPosts -->
