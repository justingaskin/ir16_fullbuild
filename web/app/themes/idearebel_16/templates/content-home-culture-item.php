<?php

//grab all the home culture
$params = array(
        'limit' => -1,
		'orderby' => 'display_order ASC'
    );

$pod = pods('home_culture', $params);

$home_cult = array();

$pod_num = 0;

while ( $pod->fetch() ) {

  $home_cult[$pod_num] = array();

  $home_cult[$pod_num]['home_name'] = $pod->field('home_title');
  $home_cult[$pod_num]['content'] = $pod->field('home_excerpt');
  $home_cult[$pod_num]['background_color'] = $pod->field('home_background_color');

  $home_cult[$pod_num]['first_image'] = array();
  $home_cult[$pod_num]['first_image']['desktop'] = $pod->field('culture_image_desktop');
  $home_cult[$pod_num]['first_image']['mobile'] = $pod->field('culture_image_mobile_retina');


  // if(($home_cult[$pod_num]['second_image']['widescreen'] || $home_cult[$pod_num]['second_image']['desktop'] || $home_cult[$pod_num]['second_image']['tablet']  || $home_cult[$pod_num]['second_image']['mobile']) == false){
  // 	$home_cult[$pod_num]['second_image'] = false;
  // }
  // else{
  // 	$home_cult[$pod_num]['second_image'] = Roots\Sage\Extras\add_responsive_image_element(array(
		// 					"wide_url" => $home_cult[$pod_num]['second_image']['widescreen']['guid'],
		// 					"desk_url" => $home_cult[$pod_num]['second_image']['desktop']['guid'],
		// 					"tab_url" => $home_cult[$pod_num]['second_image']['tablet']['guid'],
		// 					"mob_url" => $home_cult[$pod_num]['second_image']['mobile']['guid']
		// 				), $home_cult[$pod_num]['home_name'] . ' secondary image', 'homeItem_secondImage');
  // }

  $home_cult[$pod_num]['first_image'] = Roots\Sage\Extras\add_responsive_image_element(array(
							"desk_url" => $home_cult[$pod_num]['first_image']['desktop']['guid'],
							"mob_url" => $home_cult[$pod_num]['first_image']['mobile']['guid']
						), $home_cult[$pod_num]['home_name'] . ' main image', 'homeItem_firstImage');

  $pod_num++;
}

?>
<?php $index = 0; ?>
<?php $para = 0; ?>
<?php $anim = 0; ?>
<?php foreach($home_cult as $service){ ?>
	<div id="home_primContAnim" class="homeItem_primaryContainer col-xs-12">
		<div class="homeItem_descriptionWrapper col-xs-12 col-sm-6">
			<div class="homeItem_iconCTN"><img class="homeItem_icon" src="<?php print $service['icon']['guid']; ?>"></div>
			<h3 class="homeItem_name"><?php print $service['home_name']; ?></h3>
			<p class="homeItem_description"><?php print $service['content']; ?></p>
		</div>
		<div class="homeItem_imageWrapper col-xs-12 col-sm-6">
			<?php print $service['first_image']; ?>
		</div>
		
		</div>
	</div>
<?php }?>