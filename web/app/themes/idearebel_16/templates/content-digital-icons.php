
	<?php
	$params = array(
	        'limit' => -1,
	        'orderby' => 'display_order'
	    );

	$pod = pods('digitalicon', $params);

	$digital_icons = array();

	$pod_num = 0;

	while ( $pod->fetch() ) {

	  $digital_icons[$pod_num] = array();
	  $digital_icons[$pod_num]['name'] = $pod->field('digital_icon_name');
	  $digital_icons[$pod_num]['image'] = $pod->field('digital_icon_image');

	  $pod_num++;
	}
	?>
	<div id="trig__1"></div>
	<div id="anim__1">
		<section class="digitalIcons ir-content-margin col-xs-12">
			<?php foreach($digital_icons as $digital_icon){ ?>
				<article class="digitalIcon col-xs-6 col-md-3">
					<div class="digitalIcon_top col-xs-12">
						<?php
							if($digital_icon['image']){
								print '<img class"digitalIcon_image" src="'. pods_image_url($digital_icon['image'], 'thumbnail') .'" >';
							}
						?>
					</div>
					<div class="digitalIcon_bottom col-xs-12">
						<?php
							if($digital_icon['name']){
								print '<p class="digitalIcon_name">'. $digital_icon['name'] .'</p>';
							}
						?>
					</div>
				</article>
			<?php } ?>
		</section>
	</div>