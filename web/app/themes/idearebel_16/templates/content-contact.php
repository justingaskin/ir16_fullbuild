<?php
//grab all the offices
$params = array(
        'limit' => -1,
    );

$pod = pods('office', $params);

$offices = array();

while ( $pod->fetch() ) {
  $office = array();

  $office['city'] = $pod->field('city');
  $office['city_abrev'] = $pod->field('city_abrev');

  $office['google_maps_link'] = $pod->field('google_maps_link');
  
  $office['address'] = '<div class="aboutOffice-address">' .  $pod->field('address') . '</div><span class="aboutOffice-city">' . $pod->field('city') . '</span>, <span class="aboutOffice-province">' . $pod->field('provincestate') . '</span> <span class="aboutOffice-postal">' . $pod->field('postalzip_code') . '</span><div class="aboutOffice-phone">' . $pod->field('phone_number') . '</div>';

  //get appropriate gif
  $timezone = $pod->field('timezone');
  $datetime = date_format(new \DateTime("now", new \DateTimeZone($timezone)), 'G:i');
  $sunsetTime = $pod->field('sunset');
  $sunriseTime = $pod->field('sunrise');

  if(strtotime($datetime) <= strtotime($sunriseTime) || strtotime($datetime) >= strtotime($sunsetTime)){
    $conditions = 'night';
  }
  else{
    $conditions = $pod->field('current_weather_conditions');
  }

  $office['office_gif'] = Roots\Sage\Extras\add_responsive_image_element(array(
							"wide_url" => $pod->field('office_gif_widescreen_' . $conditions)['guid'],
							"desk_url" => $pod->field('office_gif_desktop_' . $conditions)['guid'],
							"tab_url" => $pod->field('office_gif_tablet_' . $conditions)['guid'],
							"mob_url" => $pod->field('office_gif_mobile_' . $conditions)['guid']
						), 'officegif','office-gif');

  
  $offices[$office['city_abrev']] = $office;
}

//k($offices);
?>
<section class="contact-getInTouch">
  <div class="contactGetInTouch-left">
    <h2>Get in touch with us.</h2>
  </div>
  <div class="contactGetInTouch-Right">
    <div class="contact__generalInfo">
      <div class="contact_genTitle">New Business</div>
      <span class="contact_genMail"><a href="mailto:newbusiness@idearebel.com">newbusiness@idearebel.com</a></span>
      <div class="contact_genTitle">Press</div>
      <span class="contact_genMail"><a href="mailto:press@idearebel.com">press@idearebel.com</a></span>
      <div class="contact_genTitle">Everything Else</div>
      <span class="contact_genMail">1 800 bleepblorp</span>
    </div>
  </div>
</section>
<section class="contact-ourOffices">
  <h4>Our Offices</h4>
  <div class="container">
    <div class="row">
      

      <?php $index = 0; ?>
      <?php foreach($offices as $abr => $office){ ?>
         <article class="contactOffice col-xs-12 col-sm-12 col-md-4 <?php echo "contactOffice__item-$index"; $index++; ?> contactOffice-<?php print strtolower($office['city_abrev']); ?>">
             <div class="contactOffice-gif">
               <?php print $office['office_gif']; ?>
             </div>

            <div class="contactOffice__details">
                <h3 class="contactOffice-city"><?php print $office['city']; ?></h3>
                <div class="contactOffice-address">
                  <?php print $office['address']; ?>
                </div>
                <a class="ir-btn ir-btn-black ir-btn-directions button" href="<?php print $office['google_maps_link']; ?>">
                  <span>Directions</span>
                </a>
            </div>

         </article>
      <?php }?>

    </div>
  </div>
</section>