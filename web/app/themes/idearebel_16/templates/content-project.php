<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <div class="entry-content container ir-project-page-container">
      <?php the_content(); ?>
    </div>
  </article>

  <?php Roots\Sage\Extras\display_project_navigation(); ?>
<?php endwhile; ?>
