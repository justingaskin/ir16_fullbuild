<?php
$office = array();
$office_id = get_the_id();

//get all the fields for the office we need and output them
$office['city_abrev'] = get_post_meta($office_id , 'city_abrev', true);
$office['city'] = get_post_meta($office_id , 'city', true);

$office['about']['address'] = '<span class="aboutOffice-address">' .  get_post_meta($office_id , 'address', true) . '</span>,<span class="aboutOffice-city">' . get_post_meta($office_id , 'city', true) . '</span>, <span class="aboutOffice-province">' . get_post_meta($office_id , 'provincestate', true) . '</span>, <span class="aboutOffice-postal">' . get_post_meta($office_id , 'postalzip_code', true) . '</span></br><span class="aboutOffice-phone">' . get_post_meta($office_id , 'phone_number', true) . '</span></br><span class="aboutOffice-email">' . get_post_meta($office_id , 'email_address', true) . '</span>';
$office['job_introduction'] = get_post_meta($office_id , 'job_introduction', true);

//get the conditins of the city but first check if its night if its night we show night gif regardless of weather
$timezone = get_post_meta(get_the_ID() , 'timezone', true);
$datetime = date_format(new \DateTime("now", new \DateTimeZone($timezone)), 'G:i');
$sunsetTime = get_post_meta(get_the_ID() , 'sunset', true);
$sunriseTime = get_post_meta(get_the_ID() , 'sunrise', true);

if(strtotime($datetime) <= strtotime($sunriseTime) || strtotime($datetime) >= strtotime($sunsetTime)){
	$conditions = 'night';
}
else{
	$conditions = get_post_meta($office_id , 'current_weather_conditions', true);
}

$office['about']['office_gif'] = Roots\Sage\Extras\add_responsive_image_element(array(
							"wide_url" => get_post_meta($office_id , 'office_gif_widescreen_' . $conditions, true)['guid'],
							"desk_url" => get_post_meta($office_id , 'office_gif_desktop_' . $conditions, true)['guid'],
							"tab_url" => get_post_meta($office_id , 'office_gif_tablet_' . $conditions, true)['guid'],
							"mob_url" => get_post_meta($office_id , 'office_gif_mobile_' . $conditions, true)['guid']
						), 'officegif','office-gif');

$office['about']['copy'] =  get_post_meta($office_id , 'about_the_office', true);

$office['city_scape'] = Roots\Sage\Extras\add_responsive_image_element(array(
							"wide_url" => get_post_meta($office_id , 'office_image_widescreen', true)['guid'],
							"desk_url" => get_post_meta($office_id , 'office_image_desktop', true)['guid'],
							"tab_url" => get_post_meta($office_id , 'office_image_tablet', true)['guid'],
							"mob_url" => get_post_meta($office_id , 'office_image_mobile', true)['guid']
						), 'city-scape','officeCityScape');

$office['description']['header'] = get_post_meta($office_id , 'welcome_to_title', true);
$office['description']['copy'] = get_post_meta($office_id , 'welcome_to_text', true);

if((get_post_meta($office_id , 'office_image_2_widescreen', true) || get_post_meta($office_id , 'office_image_2_desktop', true) ||  get_post_meta($office_id , 'office_image_2_tablet', true)  || get_post_meta($office_id , 'office_image_2_mobile', true)) == false){
		$office['office_image']  = false;
}
else{
	$office['office_image'] = Roots\Sage\Extras\add_responsive_image_element(array(
							"wide_url" => get_post_meta($office_id , 'office_image_2_widescreen', true)['guid'],
							"desk_url" => get_post_meta($office_id , 'office_image_2_desktop', true)['guid'],
							"tab_url" => get_post_meta($office_id , 'office_image_2_tablet', true)['guid'],
							"mob_url" => get_post_meta($office_id , 'office_image_2_mobile', true)['guid']
						), 'city-scape','officeCityScape');
}

///get rebel recommended
$params = array(
    'limit' => 3,
    // Be sure to sanitize ANY strings going here
    'where'=>'recommended_city.id = ' . $office_id,
);

$rebel_recommends = pods('rebelreccomended', $params);

$recommended = array();
while ( $rebel_recommends->fetch() ) {
	$recommend = array();
  	$recommend['recommended_name'] = $rebel_recommends->field('recommended_name');
  	$recommend['recommended_address'] = $rebel_recommends->field('recommended_address');
  	$terms = get_the_terms($rebel_recommends->id(), 'rebelsrecommendtype');
  	$recommend['recommended_term'] = $terms[0]->name;
  	$recommend['recommended_image'] = Roots\Sage\Extras\add_responsive_image_element(array(
							"wide_url" => $rebel_recommends->field('recommended_image_widescreen')['guid'],
							"desk_url" => $rebel_recommends->field('recommended_image_desktop')['guid'],
							"tab_url" => $rebel_recommends->field('recommended_image_tablet')['guid'],
							"mob_url" => $rebel_recommends->field('recommended_image_mobile')['guid']
						), 'city-scape','rebelsRecommend-image');
	$recommended[] = $recommend;
}
//get the job postings for this office
$params = array(
		'limit' => -1,
        'where' => 'job_location.id = ' . $office_id,
    );

$jobpod = pods('jobpost', $params);

$jobs = array();
while ( $jobpod->fetch() ) {
	$job = array();
  	$job['job_title'] = $jobpod->field('job_title');
  	$job['job_secondary_title'] = $jobpod->field('job_secondary_title');
  	$job['job_location'] = $jobpod->field('job_location')['post_name'];
  	$job['job_location_city_abbr'] = $office['city_abrev'];
  	$job['job_url'] = get_permalink($jobpod->row['ID']);

  	$jobs[] = $job;
}
?>
<article <?php post_class(); ?> >
	<div id="office-content">
		<section class="office-aboutOffice">
			<div class="office-leftCtn">
				<div class="aboutOffice-gif"><?php print $office['about']['office_gif']; ?></div>
				</div>
				<div class="office-rightCtn">
					<h3>About the Office</h3>
					<p class="ir-italic aboutOffice-location"><?php print $office['about']['address']; ?></p>
					<p class="aboutOffice-copy .ir-bodyCopy1"><?php print $office['about']['copy']; ?></p>
				</div>
		</section>
		<section class="office-cityScape"><?php print $office['city_scape']; ?></section>
		<section class="office-cityDescritption">
			<h3 class="officecCityDescription-header"><?php print $office['description']['header']; ?></h3>
			<p class="officecCityDescription-copy"><?php print $office['description']['copy']; ?></p>
		</section>
		<?php if(count($recommended) > 0){?>
		<section class="office-rebelsRecommend container">
			<span class="office-centerTitle"><h5>Rebels Recommend:</h5></span>
			<div class="office-recCtr row">
				<?php foreach($recommended as $key => $recommend){?>
					<div class="office-rebsRec col-xs-12 col-md-4">
						<?php print $recommend['recommended_image']; ?>
						<div class="rebelsRecommend">
							<div class="rebelsRecommend-term"><?php print $recommend['recommended_term']; ?></div>
							<div class="rebelsRecommend-hyphen">-</div>
							<div class="rebelsRecommend-name ir-italic"><?php print $recommend['recommended_name']; ?></div>
							<div class="rebelsRecommend-address ir-italic"><?php print $recommend['recommended_address']; ?></div>
						</div>
					</div>
				<?php }?>
			</div>
		</section>
				<?php if($office['office_image']){?>
				<section class="office-officeImage">
					<?php print $office['office_image']; ?>
				</section>
		<?php if(count($jobs) > 0){?>
		<section class="office-jobListings">
			<div class="office-content">
				<?php }?>
				<?php }?>
				<div class="office-jobPostings">
					<h9>Current <?php print $office['city'];?> Openings</h9>
					<ul class="jobListings jobListings-<?php print strtolower($office['city']);?>">
			  			<?php foreach($jobs as $key => $listing){?>
			   			 <li class="jobListing job"><a class="jobListing-link" href="<?php print $listing['job_url']; ?>"><h3 class="jobTitle"><?php print $listing['job_title']; ?></h3><i class="jobSecondaryTitle ir-italic"><?php print $listing['job_secondary_title']; ?></i></a></li>
			 			 <?php }?>
			  		</ul>
		  		</div>
			</div>
		</section>
		<?php }?>
	</div>
</article>
