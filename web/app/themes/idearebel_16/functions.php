<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
 
$sage_includes = [
  'lib/assets.php',  // Scripts and stylesheets
  'lib/extras.php',  // Custom functions
  'lib/setup.php',   // Theme setup
  'lib/titles.php',  // Page titles
  'lib/wrapper.php', // Theme wrapper class
  'lib/shortcodes.php', // Shortcode wrapper class
  'lib/shortcodes/project.php',  // Shortcode Projects wrapper class
  'lib/shortcodes/video.php',  // Shortcode Video wrapper class
  'lib/shortcodes/work.php',  // Shortcode Work wrapper class
  'lib/shortcodes/general.php',  // Shortcode General wrapper class
  'lib/shortcodes/about.php'  // Shortcode General wrapper class
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);


