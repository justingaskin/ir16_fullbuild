<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
  // Enable features from Soil when plugin is activated
  // https://roots.io/plugins/soil/
  add_theme_support('soil-clean-up');
  add_theme_support('soil-nav-walker');
  add_theme_support('soil-nice-search');
  add_theme_support('soil-jquery-cdn');
  add_theme_support('soil-relative-urls');

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('sage', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'sage')
  ]);

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');
  
  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  add_editor_style(Assets\asset_path('styles/main.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
function widgets_init() {

  register_sidebar([
    'name'          => __('Primary', 'sage'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);

  register_sidebar([
    'name' => __('Footer', 'sage'),
    'id' => 'sidebar-footer',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget' => '</section>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
  ]);

}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
    is_front_page(),
    is_page_template('template-custom.php'),
    true,
  ]);

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets() {
  wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);
  
  wp_enqueue_style('sage/font-awesome', Assets\asset_path('styles/font-awesome.css'), false, null);
  wp_enqueue_style('fullpageCss', Assets\asset_path('../assets/styles/vendor/_jquery.fullpage.css'), false, null);
  wp_enqueue_script('sage/js/type-kit', 'https://use.typekit.net/wqp3zpa.js', ['jquery'], null);
  wp_enqueue_script('sage/js/load-type-fonts', Assets\asset_path('scripts/load-fonts.js'), ['sage/js/type-kit'], null);
  wp_enqueue_style('sage/google-font/karla', 'https://fonts.googleapis.com/css?family=Karla:400,700', false, null);
  
  wp_enqueue_script('sage/js/isotope', Assets\asset_path('scripts/jquery.isotope.js'), ['jquery'], null, true);
  
  // wp_enqueue_script('waypoints', Assets\asset_path('scripts/jquery.waypoints.js'), false, null);

  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }

  wp_enqueue_script('sage/js/footer', Assets\asset_path('scripts/footer.js'), ['jquery'], null, true);

  // wp_enqueue_script('tweenmax', Assets\asset_path('scripts/TweenMax.js'), null, true);
  // wp_enqueue_script('velocity', Assets\asset_path('scripts/velocity.min.js'), null, true);
  // wp_enqueue_script('scrollmagic', Assets\asset_path('scripts/ScrollMagic.js'), null, true);
  // wp_enqueue_script('animvelo', Assets\asset_path('scripts/animation.velocity.js'), null, true);
  // wp_enqueue_script('animation-gsap', Assets\asset_path('scripts/animation.gsap.js'), null, true);
  // wp_enqueue_script('animation-TweenLite', Assets\asset_path('scripts/TweenLite.js'), null, true);
  // wp_enqueue_script('animation-addIndicators', Assets\asset_path('scripts/debug.addIndicators.js'), true);
  // invoke scrollTo
  wp_enqueue_script('scrollto', Assets\asset_path('../assets/scripts/jquery-scrollto.js'), null, true);
  // invoke waypoints
  wp_enqueue_script('sticky', Assets\asset_path('../assets/scripts/shortcuts/sticky.min.js'), null, true);
  wp_enqueue_script('inview', Assets\asset_path('../assets/scripts/shortcuts/inview.min.js'), null, true);
  wp_enqueue_script('infinite', Assets\asset_path('../assets/scripts/shortcuts/infinite.min.js'), null, true);
  // wp_enqueue_script('waypoints-dbg', Assets\asset_path('../assets/scripts/waypoints.debug.js'), null, true);
  wp_enqueue_script('waypoints', Assets\asset_path('../assets/scripts/jquery.waypoints.js'), null, true);
  // invoke all functions being run throughout site for maximum spells and thAC0
  wp_enqueue_script('anim-scripting', Assets\asset_path('scripts/anim-scripting.js'), null, true);

  wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);
  wp_enqueue_script('sage/js/general', Assets\asset_path('scripts/general.js'), ['jquery'], null, true);
  wp_enqueue_script('sage/js/nav', Assets\asset_path('scripts/nav.js'), ['jquery'], null, true);
  
  
  // Condition to only load script on Project Page and Home Page and About Us Page.
  if (get_post_type() === 'workproject' || is_front_page() || is_page(13)) {
  	wp_enqueue_script('sage/js/project', Assets\asset_path('scripts/project.js'), ['jquery'], null, true);
  	wp_enqueue_script('sage/js/jplayer', Assets\asset_path('scripts/jquery.jplayer.js'), ['jquery'], null, true);
	wp_enqueue_script('sage/js/ir_jplayer', Assets\asset_path('scripts/video.js'), ['sage/js/jplayer'], null, true);
  }
  
  // Condition to only load script on Work Page.
  if (is_page(9)) {
  	wp_enqueue_script('sage/js/work', Assets\asset_path('scripts/work.js'), ['jquery'], null, true);
  }
  // Condition to only load script on Labs page
  if (is_page(17)) {
    wp_enqueue_script('sage/js/labs', Assets\asset_path('scripts/labs.js'), ['sage/js/isotope'], null, true);
  }
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);

//TODO: ONLY ENQUEUE REQUIRED JS FOR PAGES... Video player...
//TODO: Only enqueue videoplayer JS when needed... (through shortcode.)
// function register_videoplayer_scripts() {
	// wp_register_script('sage/js/jplayer', Assets\asset_path('scripts/jquery.jplayer.js'), ['jquery'], null, true);
	// wp_register_script('sage/js/ir_jplayer', Assets\asset_path('scripts/video.js'), ['sage/js/jplayer'], null, true);
// }
// add_action('init', 'register_videoplayer_scripts', 101);
// 
// function print_videoplayer_scripts() {
	// global $add_videoplayer_scripts;
// 
	// // if ( ! $add_videoplayer_scripts )
		// // return;
// 		
	// wp_print_scripts('sage/js/jplayer');
	// wp_print_scripts('sage/js/ir_jplayer');
// }
// add_action('wp_footer', 'print_videoplayer_scripts');

function admin_scripts() {
  	wp_enqueue_script('sage/js/admin', Assets\asset_path('scripts/admin.js'), ['jquery'], null, true);
}
add_action('admin_enqueue_scripts', __NAMESPACE__ . '\\admin_scripts');


