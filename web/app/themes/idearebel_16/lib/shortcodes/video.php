<?php 

namespace Roots\Sage\Shortcodes;

/**
 * Shortcode-Render - Displays an embedded IR video.
 *			
 * A shortcode to display a video using jPlayer with a video element, play elements and a video control box.
 * 
 * @since 1.0.0
 *
 * @param array $params {
 *     @type int $id The id of the video player, will be appended to video player HTML element.
 *     @type string $video_url The url of the video to be loaded.
 *     @type string $thumb_url The teaser image of the video, to placehold the video.
 *     @type string $title The title of the video (Displayed on Pause).
 *     @type string $subtitle The subtitle of the video (Displayed on Pause).
 *     @type string $width A hardcoded width for the video.
 *     @type string $height A hardcoded height for the video.
 * }
 * @param string $content Contains the content of the shortcode.
 */
function ir_render_embedded_video($params = array()) {
	// default parameters
	extract(shortcode_atts(array(
		'id' => '-1',
		'class' => '',
		'video_url' => '',
		'thumb_url' => '',
		'thumb_url_wide' => '',
		'thumb_url_desk' => '',
		'thumb_url_tab' => '',
		'thumb_url_mob' => '',
		'title' => '',
		'subtitle' => '',
		'width' => '',
		'height' => '',
		'is_header' => false,
		'front_page' => false
	), $params));
	
	// if(empty($thumb_url_wide)) {
		// $thumb_url_wide = $thumb_url;
	// }
	// if(empty($thumb_url_desk)) {
		// $thumb_url_desk = $thumb_url_wide;
	// }
	// if(empty($thumb_url_tab)) {
		// $thumb_url_tab = $thumb_url_desk;
	// }
	// if(empty($thumb_url_mob)) {
		// $thumb_url_mob = $thumb_url_tab;
	// }
	
	if($id === '-1') {
		$id = rand(0, 9999999);
	}
	$has_title = (!empty($title) || !empty($subtitle));
	$width_output = "max_width='" . $width . "';";
	$height_output = "max_height='" . $height . "';";
	if($has_title) {
		$print_title = '<div class="jp-gui-play"><div class="jp-video-title">
					      <h5>' . $title . '</h5>-
					    </div>
					    <div class="jp-video-subtitle body-copy-2">
					      ' . $subtitle . '
					    </div>';
		$print_title_mobile = '<div class="jp-gui-play-mobile"><div class="jp-video-title">
					      <h5>' . $title . '</h5>-
					    </div>
					    <div class="jp-video-subtitle body-copy-2">
					      ' . $subtitle . '
					    </div>';
	}
	//. $thumb_url_wide . ', ' . $thumb_url_desk . ', ' . $thumb_url_tab . ', ' . $thum_url_mob . '

	$output_html = '
		<div class="jp-container ir-content-padding ' . $class . ' col-xs-12">
			<div id="jp_container_' . $id .'" class="jp-video jp_container_' . $id .'" role="application" aria-label="media player" thumb_url="'
			. $thumb_url . '" video_url="' . $video_url . '"' . (($is_header) ? ' is_header' : '') . (($front_page) ? ' front_page' : '') . ' ' . ((!empty($width) ? $width_output : "" )) . ' ' .  ((!empty($height) ? $height_output : "" )) . '>
			  <div class="jp-type-single">
			    <div id="jquery_jplayer_1" class="jp-jplayer"></div>
			    <div class="jp-gui">
			      ' . (($has_title) ? $print_title : "") . '
			      <div class="jp-video-play">
			        <button class="jp-video-play-icon jp-button" role="button" tabindex="0"><i class="fa fa-play"></i></button>
			      </div>
			      ' . (($has_title) ? "</div>" : "") . '
			    </div>
			    <div class="jp-gui-pause">
            		<button class="jp-pause jp-button" role="button" tabindex="0"><i class="fa fa-pause"></i></button>
            	</div>
			    <div class="jp-no-solution">
			      <span>Update Required</span>
			      To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
			    </div>
			  </div>
		      <div class="jp-gui-mobile visible-xs hidden">
		        ' . (($has_title) ? $print_title_mobile : "") . '
		        ' . (($has_title) ? "</div>" : "") . '
		      </div>
			</div>
		</div>';

	return $output_html;
}
?>