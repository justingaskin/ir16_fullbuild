<?php

namespace Roots\Sage\Shortcodes;

use Roots\Sage\Extras as Extras;
/**
 * Shortcode-Render - Displays the Leadership section of About Us.
 *			
 * A shortcode to display the Leadership section of About Us. Appends a <h2>Leadership</h2> to the top.
 *		
 * @since 1.0.0
 *
 * @param array $params {
 *     @type string $class CSS classes that should append to this html item.
 * }
 * @param string $content Contains the content of the shortcode.
 */
function ir_render_leadership($params = array(), $content = null) {
	// default parameters
	extract(shortcode_atts(array(
		'class' => ''
	), $params));

	$output_value = "<div class='ir-page-ending-container ir-leadership-container ir-content-padding col-xs-12 $class'>";
	
	//Top Content
	$output_value .= "<div class='ir-leadership-description body-copy-1 col-xs-12'>";
	$output_value .= "<h3>" . __('Leadership') . "</h3>" . $content;
	$output_value .= "</div>";
	
	//Leadership
	$args = array('post_type' => 'teammember',
					'meta_query' => array(
			        	array('key' => 'rebel_leadership',
			            	'value' => true,
			            	'compare' => '=')
					),
					'orderby' => 'meta_value_num',
					'meta_key'  => 'display_order',
					'order' => 'ASC');
	$leadership_query = new \WP_Query($args);
	$output_value .= "<div class='ir-leadership-members col-xs-12'>";
	$post_count = $leadership_query->found_posts;
	while($leadership_query->have_posts()) {
		$leadership_query->the_post();
		$leadership_post = get_post(get_the_ID());
		$output_value .= "<div class='ir-leadership-member col-xs-12 col-sm-6 col-md-4'>";
		$output_value .= "<div class='ir-leader-picture'>";
		$widescreen_picture = $leadership_post->rebel_picture_widescreen;
		if(!empty($widescreen_picture)) {
			$widescreen_picture = $widescreen_picture['guid'];
		}
		$desktop_picture = $leadership_post->rebel_picture_desktop;
		if(!empty($desktop_picture)) {
			$desktop_picture = $desktop_picture['guid'];
		}
		$tablet_picture = $leadership_post->rebel_picture_tablet;
		if(!empty($tablet_picture)) {
			$tablet_picture = $tablet_picture['guid'];
		}
		$mobile_picture = $leadership_post->rebel_picture_picture;
		if(!empty($mobile_picture)) {
			$mobile_picture = $mobile_picture['guid'];
		}
		$output_value .= Extras\add_responsive_image_element(array(
							"wide_url" => $widescreen_picture,
							"desk_url" => $desktop_picture,
							"tab_url" => $tablet_picture,
							"mob_url" => $mobile_picture
						),
						get_the_title(), 'rebelEmployee_Image');
		$output_value .= "</div>";
		$output_value .= "<div class='ir-leader-info'>";
		$output_value .= "<div class='body-copy-1'>" . $leadership_post->post_title . "</div>";
		$output_value .= "<div class='ir-leadership-jobtitle body-copy-2'>" . $leadership_post->rebel_job_title . "</div>";
		$output_value .= "</div>";
		$output_value .= "</div>";
	}
	$output_value .= "</div>";
	
	//Bottom Content
	// $output_value .= '<div class="ir-leadership-our-story col-xs-12">';
	// $output_value .= "<h3>" . __('Learn how Idea Rebel came to be.') . "</h3>";
	//TODO: ADD Proper Link. NOT #
	// $output_value .= do_shortcode('[ir-button title="READ OUR STORY" href="#"]');
	// $output_value .= "</div>";
	// $output_value .= "</div>";
	
	wp_reset_postdata();
	
	return do_shortcode($output_value);
}

/**
 * Shortcode-Render - Displays the the preview of Idea Rebel Services page..
 *			
 * A shortcode to display the Display the preview of the services page. Takes an input array and prints out the Services preview section.
 * 
 * @since 1.0.0
 *
 * @param array $params {
 *     @type string $services A string array which is delimited by a comma. Ex. Strategy, Marketing, Creative, etc.
 * }
 * @param string $content Contains the content of the shortcode.
 */
function ir_render_services_preview($params = array()) {
	// default parameters
	extract(shortcode_atts(array(
		'services' => 'Strategy,Creative,Development,Marketing'
	), $params));
	
	$service_array = explode(',', $services);
	$service_params = array(
        'limit' => -1,
    );
	
	$pod = \pods('service', $service_params);
	
	$services = array();
	
	$pod_num = 0;
	
	for ($i = 0; $i < $pod->total(); $i++) {
		$services[$i] = array();
	}
	
	while ( $pod->fetch() ) {
		$array_position = array_search($pod->field('service_category'), $service_array);
		if($array_position !== false) {
			$services[$array_position]['service_icon'] = $pod->field('service_icon');
			$services[$array_position]['service_category'] = $pod->field('service_category');
			$services[$array_position]['service_keywords'] = explode(PHP_EOL, $pod->field('service_keywords'));
		}
		$pod_num++;
	}
	
	$output_value = "<div class='ir-services-preview col-xs-12'>";
	$output_value .= "<h7 class='ir-side-header-mobile visible-xs hidden'>" . __('What We Do') . "</h7>";
	
	for ($i = 0; $i < $pod_num; $i++) {
		$output_value .= "<div class='ir-services-preview-item ir-services-preview-" . $services[$i]['service_category'] ." col-xs-12'>";
		$output_value .= "<div class='ir-services-preview-info col-xs-12 col-sm-6 col-sm-push-6'>";
		$output_value .= "<h3>" . $services[$i]['service_category'] . "</h3>";
		$output_value .= "<div class='ir-services-keywords body-copy-2'>";
		foreach($services[$i]['service_keywords'] as $keyword) {
			$output_value .= $keyword . "<br />";
		}
		$output_value .= "</div>";
		$output_value .= "</div>";
		$output_value .= "<div class='ir-services-preview-icon col-xs-12 col-sm-6 col-sm-pull-6'>";
		$output_value .= "<div class='ir-services-icon-container'>";
		$output_value .= '<img class="ir-services-icon-img" src="' . $services[$i]['service_icon']['guid'] . '" />';
		$output_value .= "</div>";
		$output_value .= "</div>";
		$output_value .= "<h7 class='ir-side-header hidden-xs'>" . __('What We Do') . "</h7>";
		$output_value .= "<div class='ir-services-preview-item-bg'></div>";
		$output_value .= "</div>";
	}
	
	$output_value .= '<div class="ir-service-preview-link col-xs-12">';
	$output_value .= do_shortcode('[ir-button title="MORE ABOUT OUR SERVICES" href="/services/"]');
	$output_value .= "</div>";
	
	$output_value .= "</div>";
	return $output_value;
}
?>