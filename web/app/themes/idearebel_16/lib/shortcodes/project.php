<?php 

namespace Roots\Sage\Shortcodes;

/**
 * Shortcode-Render - Displays the Project Post ~~Background~~.
 *			
 * A shortcode to display the Project Background information. Appends a <h2>Background</h2> to the top.
 *		
 * @since 1.0.0
 *
 * @param array $params {
 *     @type string $post_slug A unique name that adds an css class to the section.
 *     @type int $img_id The id of an image to be displayed in this section.
 *     @type string $img_name The name of an image to be displayed in this section.
 *     @type string $img_href The href of an image to be displayed in this section.
 *     @type string $align How this section aligns - center, left, right.
 *     @type string $text_size Bootstrap class size to apply Default - col-sm-6.
 *     @type string $text_align How the text will be align (left || center || right) Default is left
 *     @type string $max_size The maximum size of the container.
 * }
 * @param string $content Contains the content of the shortcode.
 */
function ir_render_project_background($params = array(), $content = null) {
	// default parameters
	extract(shortcode_atts(array(
		'post_slug' => 'post_slug',
		'img_wide' => '',
		'img_desk' => '',
		'img_tab' => '',
		'img_mob' => '',
		'img_alt' => '',
		'img_class' => '',
		'align' => 'left',
		'text_size' => 'col-sm-6',
		'text_align' => 'left',
		'max_size' => '12',
		'parallax' => false,
		'parallax_mobile' => false
	), $params));
	
	$img_size = $text_size;
	$size_a = explode('-', $text_size);
	$is_xs = false;
	if(count($size_a) > 1) {
		$size_text = intval($size_a[2]);
		if($size_text >= 12) {
			$size_img = 12;
		} else {
			$size_img = 12 - $size_text;
		}
		$img_size = $size_a[0] . '-' . $size_a[1] . '-' . $size_img;
		if($align === 'right') {
			$left_pull = 'col-' . $size_a[1] . '-push-' . $size_img; 
			$right_push = 'col-' . $size_a[1] . '-pull-' . $size_text;
		} else if($align === 'left') {
			$left_pull = '';
			$right_push = '';
		}
		$is_xs = ($size_a[1] === 'xs');
	}
	$align_opposite = (($align === 'right') ? 'left' : (($align === 'left') ? 'right' : $align));
	
	//Specific change for arcteryx
	// if($post_slug === 'arcteryx') {
	    // $content =  preg_replace(esc_html("</p> <p>"), esc_html("</span>"), $content, 1);
		// $content = "<span class='arcteryx-span-extend'>" . $content;
	// }
	$content = "<h3>Background</h3>" . $content;
	if(empty($img_desk)) {
		$img_desk = $img_wide;
	}
	if(empty($img_tab)) {
		$img_tab = $img_desk;
	}
	if(empty($img_mob)) {
		$img_mob = $img_tab;
	}
	$output_value = "<div class='ir-project-background-container ir-content-padding col-xs-$max_size $post_slug-bg " . (($parallax) ? 'ir-floating-container' : '') . "'>";
	if($align === 'center') {
		$output_value .= "<div class='ir-project-background-description ir-align-$align body-copy-1'>". $content . "</div>";
		$output_value .= "<div class='ir-project-background-image ir-align-$align_opposite'>";
		$output_value .= \Roots\Sage\Extras\add_responsive_image_element(array(
											"wide_url" => $img_wide,
											"desk_url" => $img_desk,
											"tab_url" => $img_tab,
											"mob_url" => $img_mob
										), $img_alt, 'ir-section-image ' . $img_class);
		$output_value .= "</div>";
	} else {
		$output_value .= "<div class='ir-project-background-description ir-align-$align ir-text-$text_align body-copy-1 $text_size " . " $left_pull " . (($parallax) ? 'ir-floating-item' : '') 
		. " " . (($parallax_mobile) ? 'ir-floating-item-mobile' : '' ) . "'>". $content . "</div>";
		$output_value .= "<div class='ir-project-background-image ir-align-$align_opposite $img_size " . " $right_push'>";
		$output_value .= \Roots\Sage\Extras\add_responsive_image_element(array(
											"wide_url" => $img_wide,
											"desk_url" => $img_desk,
											"tab_url" => $img_tab,
											"mob_url" => $img_mob
										), $img_alt, 'ir-section-image ' . $img_class);
		$output_value .=  "</div>";
	}
	$output_value .= "</div>";
	return do_shortcode($output_value);
}

/**
 * Shortcode-Render - Displays the Project Post ~~Idea~~.
 *			
 * A shortcode to display the Project Idea information. Appends a <h2>The Idea</h2> to the top.
 *		
 * @since 1.0.0
 *
 * @param array $params {
 *     @type string $post_slug A unique name that adds an css class to the section.
 *     @type int $img_id The id of an image to be displayed in this section.
 *     @type string $img_name The name of an image to be displayed in this section.
 *     @type string $img_href The href of an image to be displayed in this section.
 *     @type string $align How this section aligns - center, left, right.
 *     @type string $text_size Bootstrap class size to apply Default - col-sm-6.
 *     @type string $text_align How the text will be align (left || center || right) Default is left
 *     @type string $max_size The maximum size of the container.
 * }
 * @param string $content Contains the content of the shortcode.
 */
function ir_render_project_idea($params = array(), $content = null) {
	// default parameters
	extract(shortcode_atts(array(
		'post_slug' => 'post_slug',
		'img_wide' => '',
		'img_desk' => '',
		'img_tab' => '',
		'img_mob' => '',
		'img_alt' => '',
		'img_class' => '',
		'shortcode' => '',
		'align' => 'left',
		'text_size' => 'col-sm-6',
		'text_align' => 'left',
		'max_size' => '12',
		'parallax' => false,
		'parallax_mobile' => false
	), $params));
	
	$img_size = $text_size;
	$size_a = explode('-', $text_size);
	$is_xs = false;
	
	$special_yokohama_append = '';
	
	//Append special background for yoko images
	if($post_slug === 'yokohama') {
		$special_yokohama_append = '<div class="ir-background-fill-yokohama"></div>';
	}
	
	if(count($size_a) > 1) {
		$size_text = intval($size_a[2]);
		if($size_text >= 12) {
			$size_img = 12;
		} else {
			$size_img = 12 - $size_text;
		}
		$img_size = $size_a[0] . '-' . $size_a[1] . '-' . $size_img;
		if($align === 'right') {
			$left_pull = 'col-' . $size_a[1] . '-push-' . $size_img; 
			$right_push = 'col-' . $size_a[1] . '-pull-' . $size_text;
		} else if($align === 'left') {
			$left_pull = '';
			$right_push = '';
		}
		$is_xs = ($size_a[1] === 'xs');
	}
	//Converse Special Case Idea...
	if($post_slug === 'bmw') {
		$left_pull = 'col-sm-8 col-sm-push-4 col-md-6 col-md-push-6';
		$right_push = 'col-sm-pull-0 col-md-pull-6';
	} else if($post_slug === 'yokohama') {
		$left_pull = 'col-sm-push-0 col-md-pull-6';
		$right_push = 'col-sm-pull-0 col-md-push-6';
	}
	
	$align_opposite = (($align === 'right') ? 'left' : (($align === 'left') ? 'right' : $align));
	
	$content = "<h3>The Idea</h3>" . $content;
	if(empty($img_desk)) {
		$img_desk = $img_wide;
	}
	if(empty($img_tab)) {
		$img_tab = $img_desk;
	}
	if(empty($img_mob)) {
		$img_mob = $img_tab;
	}
	$output_value = "<div class='ir-project-idea-container ir-content-padding col-xs-$max_size $post_slug-idea " . (($parallax) ? 'ir-floating-container' : '') . "'>";
	if($align === 'center') {
		$output_value .= "<div class='ir-project-idea-description ir-align-$align body-copy-1 col-xs-12'>". $content . "</div>";
		if(!empty($img_wide)) {
			$output_value .= "<div class='ir-project-idea-image ir-align-$align_opposite col-xs-12'>";
			$output_value .= \Roots\Sage\Extras\add_responsive_image_element(array(
											"wide_url" => $img_wide,
											"desk_url" => $img_desk,
											"tab_url" => $img_tab,
											"mob_url" => $img_mob
										), $img_alt, 'ir-section-image ' . $img_class);
			$output_value .= "</div>";
		} else if(!empty($shortcode)){
			$output_value .= do_shortcode('[' . $shortcode . ']');	
		}
	} else if($post_slug === 'yokohama') {
		$output_value .= "<div class='ir-project-idea-image ir-align-$align_opposite $img_size " . (($is_xs) ? '' : 'col-xs-12') . " $right_push'>";
		if(!empty($img_wide)) {
			$output_value .= \Roots\Sage\Extras\add_responsive_image_element(array(
											"wide_url" => $img_wide,
											"desk_url" => $img_desk,
											"tab_url" => $img_tab,
											"mob_url" => $img_mob
										), $img_alt, 'ir-section-image ' . $img_class);
		} else if(!empty($shortcode)){
			$output_value .= do_shortcode('[' . $shortcode . ']');	
		}
		$output_value .= "</div>";
		$output_value .= "<div class='ir-project-idea-description ir-align-$align ir-text-$text_align body-copy-1 $text_size " . (($is_xs) ? '' : 'col-xs-12') . " $left_pull " . (($parallax) ? 'ir-floating-item' : '') 
		. " " . (($parallax_mobile) ? 'ir-floating-item-mobile' : '' ) . "'>". $content . "</div>";
	} else {
		$output_value .= "<div class='ir-project-idea-description ir-align-$align ir-text-$text_align body-copy-1 $text_size " . (($is_xs) ? '' : 'col-xs-12') . " $left_pull " . (($parallax) ? 'ir-floating-item' : '') 
		. " " . (($parallax_mobile) ? 'ir-floating-item-mobile' : '' ) . "'>". $content . "</div>";
		$output_value .= "<div class='ir-project-idea-image ir-align-$align_opposite $img_size " . (($is_xs) ? '' : 'col-xs-12') . " $right_push'>";
		if(!empty($img_wide)) {
			$output_value .= \Roots\Sage\Extras\add_responsive_image_element(array(
											"wide_url" => $img_wide,
											"desk_url" => $img_desk,
											"tab_url" => $img_tab,
											"mob_url" => $img_mob
										), $img_alt, 'ir-section-image ' . $img_class);
		} else if(!empty($shortcode)){
			$output_value .= do_shortcode('[' . $shortcode . ']');	
		}
		$output_value .= "</div>";
	}
	$output_value .= $special_yokohama_append;
	$output_value .= "</div>";
	return do_shortcode($output_value);
}

/**
 * Shortcode-Render - Displays the Project Post ~~Results~~.
 *			
 * A shortcode to display the Project Results information. Appends a <h2>The Results</h2> to the top.
 *		
 * @since 1.0.0
 *
 * @param array $params {
 *     @type string $post_slug A unique name that adds an css class to the section.
 *     @type int $img_id The id of an image to be displayed in this section.
 *     @type string $img_name The name of an image to be displayed in this section.
 *     @type string $img_href The href of an image to be displayed in this section.
 *     @type int $award The id of the award to be displayed in this section.
 *     @type string $align How this section aligns - center, left, right.
 *     @type string $text_size Bootstrap class size to apply Default - col-sm-6.
 *     @type string $text_align How the text will be align (left || center || right) Default is left
 *     @type string $max_size The maximum size of the container.
 * }
 * @param string $content Contains the content of the shortcode.
 */
function ir_render_project_result($params = array(), $content = null) {
	// default parameters
	extract(shortcode_atts(array(
		'post_slug' => 'post_slug',
		'img_wide' => '',
		'img_desk' => '',
		'img_tab' => '',
		'img_mob' => '',
		'img_alt' => '',
		'img_class' => '',
		'award' => '',
		'align' => 'left',
		'text_size' => 'col-md-6',
		'text_align' => 'left',
		'max_size' => '12',
		'parallax' => false,
		'parallax_mobile' => false
	), $params));
	
	$img_size = $text_size;
	$size_a = explode('-', $text_size);
	$is_xs = false;
	if(count($size_a) > 1) {
		$size_text = intval($size_a[2]);
		if($size_text >= 12) {
			$size_img = 12;
		} else {
			$size_img = 12 - $size_text;
		}
		$img_size = $size_a[0] . '-' . $size_a[1] . '-' . $size_img;
		if($align === 'right') {
			$left_pull = 'col-' . $size_a[1] . '-push-' . $size_img; 
			$right_push = 'col-' . $size_a[1] . '-pull-' . $size_text;
		} else if($align === 'left') {
			$left_pull = '';
			$right_push = '';
		}
		$is_xs = ($size_a[1] === 'xs');
	}
	
	$align_opposite = (($align === 'right') ? 'left' : (($align === 'left') ? 'right' : $align));
	
	$content = "<h3>Results</h3>" . $content;
	$show_award = '';
	if(!empty($award)) {
		$show_award = do_shortcode('[display-award id="' . $award . '"]');
	}
	if(empty($img_desk)) {
		$img_desk = $img_wide;
	}
	if(empty($img_tab)) {
		$img_tab = $img_desk;
	}
	if(empty($img_mob)) {
		$img_mob = $img_tab;
	}
	$output_value = "<div class='ir-project-result-container col-xs-$max_size $post_slug-result " . (($parallax) ? 'ir-floating-container' : '') . "'>";
	if($align === 'center') {
		$output_value .= "<div class='ir-project-result-description ir-align-$align ir-text-$text_align body-copy-1 col-xs-12'>". $content . "</div>";
		if(!empty($show_award)) {
			$output_value .= "<div class='ir-project-result-image ir-align-$align_opposite col-xs-12'>";
			$output_value .= $show_award;
			$output_value .= "</div>";
		} else if(!empty($img_wide)) {
			$output_value .= "<div class='ir-project-result-image ir-align-$align_opposite col-xs-12'>";
			$output_value .= \Roots\Sage\Extras\add_responsive_image_element(array(
											"wide_url" => $img_wide,
											"desk_url" => $img_desk,
											"tab_url" => $img_tab,
											"mob_url" => $img_mob
										), $img_alt, 'ir-section-image ' . $img_class);
			$output_value .= "</div>";
		}
	} else {
		$output_value .= "<div class='ir-project-result-description ir-align-$align ir-text-$text_align body-copy-1 $text_size " . (($is_xs) ? '' : 'col-xs-12') . " $left_pull " . (($parallax) ? 'ir-floating-item' : '') 
		. " " . (($parallax_mobile) ? 'ir-floating-item-mobile' : '' ) . "'>". $content . "</div>";
		$output_value .= "<div class='ir-project-result-image ir-align-$align_opposite  $img_size " . (($is_xs) ? '' : 'col-xs-12') . " $right_push'>";
		if(!empty($show_award)) {
			$output_value .= $show_award;
		} else if(!empty($img_wide)) {
			$output_value .= \Roots\Sage\Extras\add_responsive_image_element(array(
											"wide_url" => $img_wide,
											"desk_url" => $img_desk,
											"tab_url" => $img_tab,
											"mob_url" => $img_mob
										), $img_alt, 'ir-section-image ' . $img_class);
		}
		$output_value .=  "</div>";
	
	}
	$output_value .= "</div>";
	return do_shortcode($output_value);
}

/**
 * Shortcode-Render - Displays the Project Post ~~Item~~.
 *			
 * A shortcode to display the information of a section in the project page, a Project Item.
 *		
 * @since 1.0.0
 *
 * @param array $params {
 *     @type string $title The title of the section.
 *     @type string $post_slug A unique name that adds an css class to the section.
 *     @type int $img_id The id of an image to be displayed in this section.
 *     @type string $img_name The name of an image to be displayed in this section.
 *     @type string $img_href The href of an image to be displayed in this section.
 *     @type string $align How this section aligns - center, left, right.
 *     @type string $text_size Bootstrap class size to apply Default - col-sm-6.
 *     @type string $text_align How the text will be align (left || center || right) Default is left
 *     @type string $max_size The maximum size of the container.
 *     @type string $class Additional classes to add to the element
 * }
 * @param string $content Contains the content of the shortcode.
 */
function ir_render_project_item($params = array(), $content = null) {
	// default parameters
	extract(shortcode_atts(array(
		'title' => '',
		'post_slug' => 'post_slug',
		'img_wide' => '',
		'img_desk' => '',
		'img_tab' => '',
		'img_mob' => '',
		'img_alt' => '',
		'img_class' => '',
		'img_class_container' => '',
		'align' => 'left',
		'text_size' => 'col-sm-6',
		'text_align' => 'left',
		'max_size' => '12',
		'class' => '',
		'parallax' => false,
		'parallax_mobile' => false
	), $params));
	
	$img_size = $text_size;
	$size_a = explode('-', $text_size);
	$is_xs = false;
	if(count($size_a) > 1) {
		$size_text = intval($size_a[2]);
		if($size_text >= 12) {
			$size_img = 12;
		} else {
			$size_img = 12 - $size_text;
		}
		$img_size = $size_a[0] . '-' . $size_a[1] . '-' . $size_img;
		if($align === 'right') {
			$left_pull = 'col-' . $size_a[1] . '-push-' . $size_img; 
			$right_push = 'col-' . $size_a[1] . '-pull-' . $size_text;
		} else if($align === 'left') {
			$left_pull = '';
			$right_push = '';
		}
		$is_xs = ($size_a[1] === 'xs');
	}
	
	$align_opposite = (($align === 'right') ? 'left' : (($align === 'left') ? 'right' : $align));
	
	if(empty($img_desk)) {
		$img_desk = $img_wide;
	}
	if(empty($img_tab)) {
		$img_tab = $img_desk;
	}
	if(empty($img_mob)) {
		$img_mob = $img_tab;
	}
	
	if(!empty($title)) {
		$content = "<h3>$title</h3>" . $content;	
	}
	
	$output_value = "<div id='service_trig1' class='ir-project-item-container ir-content-padding col-xs-$max_size $post_slug-item $class " . (($parallax) ? 'ir-floating-container' : '') . "'><div></div>";
	if($align === 'center') {
		if(!empty($content)) {
			$output_value .= "<div id='service_anim1'><div class='ir-project-item-description ir-align-$align ir-text-$text_align body-copy-1'>". $content . "<div id='project_item'></div> </div> </div>";
		}
		if(!empty($img_wide)) {
			$output_value .= "<div class='ir-project-item-image ir-align-$align_opposite'>";
			$output_value .= \Roots\Sage\Extras\add_responsive_image_element(array(
											"wide_url" => $img_wide,
											"desk_url" => $img_desk,
											"tab_url" => $img_tab,
											"mob_url" => $img_mob
										), $img_alt, 'ir-section-image ' . $img_class);
			$output_value .= "<div id='project_item'></div> </div>";
		}
	} else {
		$output_value .= "<div id='service_anim1'><div class='ir-project-item-description ir-align-$align ir-text-$text_align body-copy-1 $text_size " . " $left_pull " . (($parallax) ? 'ir-floating-item' : '') 
		. " " . (($parallax_mobile) ? 'ir-floating-item-mobile' : '' ) . "'>". $content . "<div id='project_item'></div> </div>";
		$output_value .= "<div class='ir-project-item-image ir-align-$align_opposite $img_size " . "  $right_push $img_class_container'>";
		if(!empty($img_wide)) {
			$output_value .= \Roots\Sage\Extras\add_responsive_image_element(array(
											"wide_url" => $img_wide,
											"desk_url" => $img_desk,
											"tab_url" => $img_tab,
											"mob_url" => $img_mob
										), $img_alt, 'ir-section-image ' . $img_class);
		}
		$output_value .= "<div id='project_item'></div> </div>";
	}
	$output_value .= "<div id='project_item'></div> </div>";
	return do_shortcode($output_value);
}
 
/**
 * Shortcode-Render - Displays an award.
 *			
 * A shortcode to display an award section.
 * 
 * @since 1.0.0
 *
 * @param array $params {
 *     @type int $id The id of the award to be displayed.
 * }
 */
function ir_render_display_award($params = array()) {
	// default parameters
	extract(shortcode_atts(array(
		'id' => '-1'
	), $params));
	
	$output_value = '';
	if($id != -1) {
		$output_value .= '<div class="ir-award-container col-xs-12 col-sm-offset-6 col-sm-6 col-md-offset-0  col-md-12">';
		$bg_image = get_post_meta($id, 'award_icon', true);
		if(!empty($bg_image)) {
			$output_value .= "<img class='ir-award-image' src='" . $bg_image['guid'] . "' />";
			$award_type = get_post_meta($id, 'award_type', true);
			if(!empty($award_type) && $award_type === 'fwa') {
				$output_value .= "<h5>Mobile of the Day</h5>";
			}
		}
	}
	$output_value .= '</div>';
	
	return $output_value;
}

/**
 * Shortcode-Render - Displays the VR Hover Javascript Module.
 *			
 * A shortcode to display the VR Hover Javascript Module. On mouse hover, depending on position it displays a different image
 * there are 5 images: center, left, right, bottom, top
 * 
 * @since 1.0.0
 * 
 */
function ir_render_vr_image_hover() {

	$extention = '.jpg';
	$wide = "widescreen";
	$desk = "desktop";
	$tab = "tablet";
	$mob = "mobile";
	
	$center = esc_url(\get_template_directory_uri()) . '/assets/images/converse-vr-center-';
	$up_1 = esc_url(\get_template_directory_uri()) . '/assets/images/converse-vr-up-1-';
	$up_2 = esc_url(\get_template_directory_uri()) . '/assets/images/converse-vr-up-2-';
	$right_1 = esc_url(\get_template_directory_uri()) . '/assets/images/converse-vr-right-1-';
	$right_2 = esc_url(\get_template_directory_uri()) . '/assets/images/converse-vr-right-2-';
	$right_3 = esc_url(\get_template_directory_uri()) . '/assets/images/converse-vr-right-3-';
	$down_1 = esc_url(\get_template_directory_uri()) . '/assets/images/converse-vr-down-1-';
	$down_2 = esc_url(\get_template_directory_uri()) . '/assets/images/converse-vr-down-2-';
	$left_1 = esc_url(\get_template_directory_uri()) . '/assets/images/converse-vr-left-1-';
	$left_2 = esc_url(\get_template_directory_uri()) . '/assets/images/converse-vr-left-2-';
	$left_3 = esc_url(\get_template_directory_uri()) . '/assets/images/converse-vr-left-3-';
	
	$center = \Roots\Sage\Extras\add_responsive_image_element(array(
											"wide_url" => $center . $wide . $extention,
											"desk_url" => $center . $desk . $extention,
											"tab_url" => $center . $tab . $extention,
											"mob_url" => $center . $mob . $extention
										), null, 'ir-vr-image');
	$up_1 = \Roots\Sage\Extras\add_responsive_image_element(array(
											"wide_url" => $up_1 . $wide . $extention,
											"desk_url" => $up_1 . $desk . $extention,
											"tab_url" => $up_1 . $tab . $extention,
											"mob_url" => $up_1 . $mob . $extention
										), null, 'ir-vr-image');
	$up_2 = \Roots\Sage\Extras\add_responsive_image_element(array(
											"wide_url" => $up_2 . $wide . $extention,
											"desk_url" => $up_2 . $desk . $extention,
											"tab_url" => $up_2 . $tab . $extention,
											"mob_url" => $up_2 . $mob . $extention
										), null, 'ir-vr-image');
	$right_1 = \Roots\Sage\Extras\add_responsive_image_element(array(
											"wide_url" => $right_1 . $wide . $extention,
											"desk_url" => $right_1 . $desk . $extention,
											"tab_url" => $right_1 . $tab . $extention,
											"mob_url" => $right_1 . $mob . $extention
										), null, 'ir-vr-image');
	$right_2 = \Roots\Sage\Extras\add_responsive_image_element(array(
											"wide_url" => $right_2 . $wide . $extention,
											"desk_url" => $right_2 . $desk . $extention,
											"tab_url" => $right_2 . $tab . $extention,
											"mob_url" => $right_2 . $mob . $extention
										), null, 'ir-vr-image');
	$right_3 = \Roots\Sage\Extras\add_responsive_image_element(array(
											"wide_url" => $right_3 . $wide . $extention,
											"desk_url" => $right_3 . $desk . $extention,
											"tab_url" => $right_3 . $tab . $extention,
											"mob_url" => $right_3 . $mob . $extention
										), null, 'ir-vr-image');
	$down_1 = \Roots\Sage\Extras\add_responsive_image_element(array(
											"wide_url" => $down_1 . $wide . $extention,
											"desk_url" => $down_1 . $desk . $extention,
											"tab_url" => $down_1 . $tab . $extention,
											"mob_url" => $down_1 . $mob . $extention
										), null, 'ir-vr-image');
	$down_2 = \Roots\Sage\Extras\add_responsive_image_element(array(
											"wide_url" => $down_2 . $wide . $extention,
											"desk_url" => $down_2 . $desk . $extention,
											"tab_url" => $down_2 . $tab . $extention,
											"mob_url" => $down_2 . $mob . $extention
										), null, 'ir-vr-image');
	$left_1 = \Roots\Sage\Extras\add_responsive_image_element(array(
											"wide_url" => $left_1 . $wide . $extention,
											"desk_url" => $left_1 . $desk . $extention,
											"tab_url" => $left_1 . $tab . $extention,
											"mob_url" => $left_1 . $mob . $extention
										), null, 'ir-vr-image');
	$left_2 = \Roots\Sage\Extras\add_responsive_image_element(array(
											"wide_url" => $left_2 . $wide . $extention,
											"desk_url" => $left_2 . $desk . $extention,
											"tab_url" => $left_2 . $tab . $extention,
											"mob_url" => $left_2 . $mob . $extention
										), null, 'ir-vr-image');
	$left_3 = \Roots\Sage\Extras\add_responsive_image_element(array(
											"wide_url" => $left_3 . $wide . $extention,
											"desk_url" => $left_3 . $desk . $extention,
											"tab_url" => $left_3 . $tab . $extention,
											"mob_url" => $left_3 . $mob . $extention
										), null, 'ir-vr-image');
	
	//2 up 2 down
	//3 left 3 right
	//1 center
	$output_value = '<div class="ir-vr-hover col-xs-12">
						<div class="ir-vr-hover-swap-container">
							<div class="ir-vr-hover-img ir-vr-hover-center">
								' . $center . '
							</div>
							<div class="ir-vr-hover-img ir-vr-hover-up-1">
								' . $up_1 . '
							</div>
							<div class="ir-vr-hover-img ir-vr-hover-up-2">
								' . $up_2 . '
							</div>
							<div class="ir-vr-hover-img ir-vr-hover-right-1">
								' . $right_1 . '
							</div>
							<div class="ir-vr-hover-img ir-vr-hover-right-2">
								' . $right_2 . '
							</div>
							<div class="ir-vr-hover-img ir-vr-hover-right-3">
								' . $right_3 . '
							</div>
							<div class="ir-vr-hover-img ir-vr-hover-down-1">
								' . $down_1 . '
							</div>
							<div class="ir-vr-hover-img ir-vr-hover-down-2">
								' . $down_2 . '
							</div>
							<div class="ir-vr-hover-img ir-vr-hover-left-1">
								' . $left_1 . '
							</div>
							<div class="ir-vr-hover-img ir-vr-hover-left-2">
								' . $left_2 . '
							</div>
							<div class="ir-vr-hover-img ir-vr-hover-left-3">
								' . $left_3 . '
							</div>
						</div>
					</div>';
	
	return $output_value;
}

/**
 * Shortcode-Render - Displays an IR-button Styled.
 *			
 * A shortcode to display an IR-button Styled
 * 
 * @since 1.0.0
 *
 * @param array $params {
 *     @type string $title The title of the button, to be displayed inside the button.
 *     @type string $color The color scheme of the button (black || white).
 *     @type string $class CSS classes to append to the element.
 *     @type string $href The url the that button will link too.
 * }
 * @param string $content Contains the content of the shortcode.
 */
function ir_render_button($params = array()) {
	// default parameters
	extract(shortcode_atts(array(
		'title' => '',
		'color' => 'black',
		'class' => '',
		'href' => ''
	), $params));
	
	$output_value = '<a class="button button--' . $color . ' ' . $class . ' ir-button" href="' . $href . '"><span>' . $title . '</span></a>';
	
	return $output_value;
}

/**
 * Shortcode - Displays the Converse chucks 4 images and hover state.
 *			
 * A shortcode to display the Converse chucks 4 images and hover state. There are 4 images,
 * with a hover state showing their title and subtitle.
 * 
 * @since 1.0.0
 *
 * @param array $params {
 *     @type type $var Description.
 *     @type type $var Description.
 * }
 * @param string $content Contains the content of the shortcode.
 */
function ir_render_converse_chucks($params = array()) {
	// default parameters
	extract(shortcode_atts(array(
	), $params));
	$output_value = '<div class="ir-converse-chucks col-xs-12">
						<div class="ir-converse-chucks-inner col-xs-12 col-md-6">
						<div class="ir-converse-shoe-container col-xs-6 ir-center">
							<div class="ir-converse-shoe-inner">
								<img class="ir-converse-shoe-icon" src="' . \get_template_directory_uri() . '/assets/images/converse-joanna.png" />
								<div class="ir-converse-content">
									<div class="ir-converse-content-inner">
										<h5>Joanna DeLane</h5>-
										<div class="body-copy-2">
											Actress
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="ir-converse-shoe-container col-xs-6 ir-center">
							<div class="ir-converse-shoe-inner">
								<img class="ir-converse-shoe-icon" src="' . \get_template_directory_uri() . '/assets/images/converse-king.png" />
								<div class="ir-converse-content">
									<div class="ir-converse-content-inner">
										<h5>King Tuff</h5>-
										<div class="body-copy-2">
											Musician
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
						<div class="ir-converse-chucks-inner col-xs-12 col-md-6">
						<div class="ir-converse-shoe-container col-xs-6 ir-center">
							<div class="ir-converse-shoe-inner">
								<img class="ir-converse-shoe-icon" src="' . \get_template_directory_uri() . '/assets/images/converse-thomas.png" />
								<div class="ir-converse-content">
									<div class="ir-converse-content-inner">
										<h5>Thomas Midlane</h5>-
										<div class="body-copy-2">
											Journalist
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="ir-converse-shoe-container col-xs-6 ir-center">
							<div class="ir-converse-shoe-inner">
								<img class="ir-converse-shoe-icon" src="' . \get_template_directory_uri() . '/assets/images/converse-ron.png" />
								<div class="ir-converse-content">
									<div class="ir-converse-content-inner">
										<h5>Ron English</h5>-
										<div class="body-copy-2">
											Artist
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>';
	
	return $output_value;
}

/**
 * Shortcode - Displays a Collaboration with Tool section.
 *			
 * A shortcode to display a Collaboration with Tool section. Typically used in Project Work Pages.
 * 
 * @since 1.0.0
 *
 * @param array $params {
 *     @type type $var Description.
 *     @type type $var Description.
 * }
 */
function ir_render_collaboration_tool($params = array()) {
	// default parameters
	extract(shortcode_atts(array(
		'align' => 'center'
	), $params));
	
	$align = 'ir-' . $align;
	// header-ir-select-button-
	$output_value = "<div class='tool-collaboration " . $align . " col-xs-12'><div class='ir-tool-wrapper col-xs-12'><h9>In Collaboration with </h9><img class='tool-icon' alt='Tool' src='"
					.\get_template_directory_uri() . "/assets/images/tool-icon.png' /></div></div>";
	
	return $output_value;
}
?>