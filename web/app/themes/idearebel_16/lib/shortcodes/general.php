<?php

namespace Roots\Sage\Shortcodes;

/**
 * Shortcode-Render - Displays the Select Clients Section of a post.
 *			
 * A shortcode to display the Clients Section on the Work Page, About Us, and Homepage. Loads all of the Clients Post type.
 *
 * @param array $params {
 *     @type boolean $front_page Should display clients on front page?
 * }
 * 
 * @since 1.0.0
 */
function ir_render_select_clients($params = array()) {
	
	// default parameters
	extract(shortcode_atts(array(
		'front_page' => false
	), $params));
	
	$output_value = "<div class='ir-select-clients-container " . (($front_page) ? " front-page-clients" : "") . "'>";
	$output_value .= "<h5 class='ir-select-clients-header'>" . __('Select Clients') . "</h5>";

	$args = array('post_type' => 'client',
				'meta_query' => array(
		        	array('key' => 'front_page_client',
		            	'value' => true,
		            	'compare' => (($front_page) ? '=' : '!=')
		        	)
				),
				'orderby' => 'meta_value_num',
				'meta_key'  => 'display_order',
				'order' => 'ASC');
				
	$clients_query = new \WP_Query($args);
	$clients_count = 0;
	
	$client_post_count = $clients_query->found_posts;
	$client_post_mod3 = $client_post_count % 3;
	$client_post_mod4 = $client_post_count % 4;
	while($clients_query->have_posts()) {
		$clients_query->the_post();
		
		$client_icon = get_post_meta(get_the_ID(), 'client_icon', true);
		if(!empty($client_icon)) {
			$client_icon = $client_icon['guid'];
		} else {
			$client_icon = "";
		}
		$apply_last_line_center = "";
		if($client_post_mod3 != 0) {
			if($client_post_mod3 == 1 && ($clients_count + 2) > $client_post_count) {
				$output_value .= "<div class='col-xs-4 visible-xs-block'></div>";
				$client_post_mod3 = 0;
			} else if($client_post_mod3 == 2 && ($clients_count + 3) > $client_post_count) {
				$output_value .= "<div class='col-xs-2 visible-xs-block'></div>";
				$client_post_mod3 = 0;
			}
		}
		if($client_post_mod4 != 0) {
			if($client_post_mod4 == 1 && (($clients_count + 2) > $client_post_count)) {
				$output_value .= "<div class='col-sm-4-5 visible-sm-block'></div>";
				$client_post_mod4 = 0;
			} else if($client_post_mod4 == 2 && (($clients_count + 3) > $client_post_count)) {
				$output_value .= "<div class='col-sm-3 visible-sm-block'></div>";
				$client_post_mod4 = 0;
			} else if($client_post_mod4 == 3 && (($clients_count + 4) > $client_post_count)) {
				$output_value .= "<div class='col-sm-1-5 visible-sm-block'></div>";
				$client_post_mod4 = 0;
			}
		}
		$output_value .= "<div class='ir-select-client-image-container $apply_last_line_center " . ((($clients_count % 5) == 0) ? "col-md-offset-1" : "") . " col-md-2 col-sm-3 col-xs-4'>";
			$output_value .= "<span class='ir-center-helper'></span><img class='ir-select-client-image' src='$client_icon'/>";
		$output_value .= "</div>";
		if(($clients_count % 5) == 4) {
			$output_value .= "<div class='col-md-1'></div>";
		}
		$clients_count++;
	}
	$output_value .= "</div>";
	
	wp_reset_postdata();
	return $output_value;
}

/**
 * Shortcode-Render - Displays the Select Awards Section of a post.
 *			
 * A shortcode to display the Awards Section on the About Us and Homepage. Loads all of the Awards Post type.
 *
 * @param array $params {
 *     @type boolean $front_page Should display Awards on front page?
 * }
 * 
 * @since 1.0.0
 */
function ir_render_select_awards($params = array()) {
	
	// default parameters
	extract(shortcode_atts(array(
		'front_page' => true
	), $params));
	
	$output_value = "<div class='ir-select-awards-container'>";
	$output_value .= "<h5 class='ir-select-awards-header'>" . __('Select Awards') . "</h5>";	
	$args = array('post_type' => 'award',
				'meta_query' => array(
		        	array('key' => 'front_page_award',
		            	'value' => true,
		            	'compare' => (($front_page) ? '=' : '!=')
		        	)
				),
				'orderby' => 'meta_value_num',
				'meta_key'  => 'display_order',
				'order' => 'ASC');
				
	$award_query = new \WP_Query($args);
	$awards_count = 0;
	
	$award_post_count = $award_query->found_posts;
	$award_post_mod2 = $award_post_count % 2;
	$award_post_mod4 = $award_post_count % 4;
	while($award_query->have_posts()) {
		$award_query->the_post();
		
		$award_icon = get_post_meta(get_the_ID(), 'award_icon', true);
		if(!empty($award_icon)) {
			$award_icon = $award_icon['guid'];
		} else {
			$award_icon = "";
		}
		$apply_last_line_center = "";
		if($award_post_mod2 != 0) {
			if($award_post_mod2 == 1 && ($awards_count + 2) > $award_post_count) {
				$output_value .= "<div class='col-xs-3 visible-xs-block'></div>";
				$award_post_mod2 = 0;
			}
		}
		if($award_post_mod4 != 0) {
			if($award_post_mod4 == 1 && (($awards_count + 2) > $award_post_count)) {
				$output_value .= "<div class='col-sm-4-5 visible-sm-block'></div>";
				$award_post_mod4 = 0;
			} else if($award_post_mod4 == 2 && (($awards_count + 3) > $award_post_count)) {
				$output_value .= "<div class='col-sm-3 visible-sm-block'></div>";
				$award_post_mod4 = 0;
			} else if($award_post_mod4 == 3 && (($awards_count + 4) > $award_post_count)) {
				$output_value .= "<div class='col-sm-1-5 visible-sm-block'></div>";
				$award_post_mod4 = 0;
			}
		}
		$output_value .= "<div class='ir-select-award-image-container $apply_last_line_center col-md-3 col-sm-3 col-xs-6'>";
			$output_value .= "<span class='ir-center-helper'></span><img class='ir-select-award-image' src='$award_icon'/>";
		$output_value .= "</div>";
		$awards_count++;
	}
	$output_value .= "</div>";
	
	wp_reset_postdata();
	return $output_value;
}

/**
 * Shortcode-Render - Displays the an ~~Item~~.
 *			
 * A shortcode to display the information of a section in the project page, a Project Item.
 *		
 * @since 1.0.0
 *
 * @param array $params {
 *     @type string $post_slug A unique name that adds an css class to the section.
 *     @type int $img_id The id of an image to be displayed in this section.
 *     @type string $img_name The name of an image to be displayed in this section.
 *     @type string $img_href The href of an image to be displayed in this section.
 * }
 * @param string $content Contains the content of the shortcode.
 */
function ir_render_item_image($params = array()) {
	// default parameters
	extract(shortcode_atts(array(
		'post_slug' => 'post_slug',
		'img_wide' => '',
		'img_desk' => '',
		'img_tab' => '',
		'img_mob' => '',
		'img_alt' => '',
		'img_class' => '',
		'class' => ''
	), $params));
	if(empty($img_wide)) {
		return '';
	}
	if(empty($img_desk)) {
		$img_desk = $img_wide;
	}
	if(empty($img_tab)) {
		$img_tab = $img_desk;
	}
	if(empty($img_mob)) {
		$img_mob = $img_tab;
	}
	$output_value = '';
	$output_value .= "<div class='ir-item-image $post_slug-item-image $class'>";
	$output_value .= \Roots\Sage\Extras\add_responsive_image_element(array(
							"wide_url" => $img_wide,
							"desk_url" => $img_desk,
							"tab_url" => $img_tab,
							"mob_url" => $img_mob
						), $img_alt, $img_class);
	$output_value .= "</div>";
	
	return do_shortcode($output_value);
}

/**
 * Shortcode-Render - Displays a basic IR Section.
 *			
 * A shortcode to display the Clients Section on the Work Page. Loads all of the Clients Post type.
 *
 * @param array $params {
 *     @type string $title Should display clients on front page?
 * 	   @type string $class the class to be appended to the container.
 * }
 * @param string $content Contains the content of the shortcode.
 * 
 * @since 1.0.0
 */
function ir_render_ir_section($params = array(), $content = null) {
	
	// default parameters
	extract(shortcode_atts(array(
		'title' => '',
		'class' => ''
	), $params));
	
	$output_value = '<div class="ir-section-item ir-content-padding col-xs-12 ' . $class . '">';
	$output_value .= '<div class="ir-section-item-inner col-xs-12">';
	$output_value .= '<h3>' . $title . '</h3>';
	$output_value .= '<div class="body-copy-1">' . $content . '</div>';
	$output_value .= '</div>';
	$output_value .= '</div>';
	
	return $output_value;
}

/**
 * A support function to retrieve image urls.
 *			
 * Retrieves the image url from a provided slug name
 * 
 * @since 1.0.0
 *
 * @param string $slug The slug that the image will be retreive for 
 */
function get_image_url_by_slug( $slug ) {
  $args = array(
    'post_type' => 'attachment',
    'name' => sanitize_title($slug),
    'posts_per_page' => 1,
    'post_status' => 'inherit',
  );
  $_header = get_posts( $args );
  $header = $_header ? array_pop($_header) : null;
  return $header ? wp_get_attachment_url($header->ID) : '';
}
 ?>
