<?php

namespace Roots\Sage\Shortcodes;

/**
 * Shortcode - Displays the Select Clients Section of a post.
 *			
 * A shortcode to display the Clients Section on the Work Page, About Us, and Homepage. Loads all of the Clients Post type.
 *		
 * @param array $params {
 *     @type boolean $front_page Should display clients on front page?
 * }
 * 
 * @since 1.0.0
 */
function ir_select_clients($params) {
	return ir_render_select_clients($params);
}
add_shortcode('select-clients', 'Roots\Sage\Shortcodes\ir_select_clients');


/**
 * Shortcode - Displays the Select Awards Section of a post.
 *			
 * A shortcode to display the Awards Section on the About Us, and Homepage. Loads all of the Awards Post type.
 *		
 * @param array $params {
 *     @type boolean $front_page Should display clients on front page?
 * }
 * 
 * @since 1.0.0
 */
function ir_select_awards($params) {
	return ir_render_select_awards($params);
}
add_shortcode('select-awards', 'Roots\Sage\Shortcodes\ir_select_awards');

/**
 * Shortcode - Displays the Project Post ~~Background~~.
 *			
 * A shortcode to display the Project Background information. Appends a <h2>Background</h2> to the top.
 *		
 * @since 1.0.0
 *
 * @param array $params {
 *     @type string $post_slug A unique name that adds an css class to the section.
 *     @type int $img_id The id of an image to be displayed in this section.
 *     @type string $img_name The name of an image to be displayed in this section.
 *     @type string $img_href The href of an image to be displayed in this section.
 *     @type string $align How this section aligns - center, left, right.
 * }
 * @param string $content Contains the content of the shortcode.
 */
function ir_project_background($params, $content) {
	return ir_render_project_background($params, $content);
}
add_shortcode('project-background', 'Roots\Sage\Shortcodes\ir_project_background');

/**
 * Shortcode - Displays the Project Post ~~Idea~~.
 *			
 * A shortcode to display the Project Idea information. Appends a <h2>The Idea</h2> to the top.
 *		
 * @since 1.0.0
 *
 * @param array $params {
 *     @type string $post_slug A unique name that adds an css class to the section.
 *     @type int $img_id The id of an image to be displayed in this section.
 *     @type string $img_name The name of an image to be displayed in this section.
 *     @type string $img_href The href of an image to be displayed in this section.
 *     @type string $align How this section aligns - center, left, right.
 * }
 * @param string $content Contains the content of the shortcode.
 */
function ir_project_idea($params, $content) {
	return ir_render_project_idea($params, $content);
}
add_shortcode('project-idea', 'Roots\Sage\Shortcodes\ir_project_idea');

/**
 * Shortcode - Displays the Project Post ~~Results~~.
 *			
 * A shortcode to display the Project Results information. Appends a <h2>The Results</h2> to the top.
 *		
 * @since 1.0.0
 *
 * @param array $params {
 *     @type string $post_slug A unique name that adds an css class to the section.
 *     @type int $img_id The id of an image to be displayed in this section.
 *     @type string $img_name The name of an image to be displayed in this section.
 *     @type string $img_href The href of an image to be displayed in this section.
 *     @type int $award The id of the award to be displayed in this section.
 *     @type string $align How this section aligns - center, left, right.
 * }
 * @param string $content Contains the content of the shortcode.
 */
function ir_project_result($params, $content) {
	return ir_render_project_result($params, $content);
}
add_shortcode('project-result', 'Roots\Sage\Shortcodes\ir_project_result');

/**
 * Shortcode - Displays the Project Post ~~Item~~.
 *			
 * A shortcode to display the information of a section in the project page, a Project Item.
 *		
 * @since 1.0.0
 *
 * @param array $params {
 *     @type string $post_slug A unique name that adds an css class to the section.
 *     @type int $img_id The id of an image to be displayed in this section.
 *     @type string $img_name The name of an image to be displayed in this section.
 *     @type string $img_href The href of an image to be displayed in this section.
 *     @type string $align How this section aligns - center, left, right.
 * }
 * @param string $content Contains the content of the shortcode.
 */
function ir_project_item($params, $content) {
	return ir_render_project_item($params, $content);
}
add_shortcode('project-item', 'Roots\Sage\Shortcodes\ir_project_item');

/**
 * Shortcode - Displays the an ~~Item-Image~~.
 *			
 * A shortcode to display the information of a section in the project page, a Project Item.
 *		
 * @since 1.0.0
 *
 * @param array $params {
 *     @type string $post_slug A unique name that adds an css class to the section.
 *     @type int $img_id The id of an image to be displayed in this section.
 *     @type string $img_name The name of an image to be displayed in this section.
 *     @type string $img_href The href of an image to be displayed in this section.
 * }
 * @param string $content Contains the content of the shortcode.
 */
function ir_item_image($params) {
	return ir_render_item_image($params);
}
add_shortcode('ir-item-image', 'Roots\Sage\Shortcodes\ir_item_image');

/**
 * Shortcode - Displays an embedded IR video.
 *			
 * A shortcode to display a video using jPlayer with a video element, play elements and a video control box.
 * 
 * @since 1.0.0
 *
 * @param array $params {
 *     @type int $id The id of the video player, will be appended to video player HTML element.
 *     @type string $video_url The url of the video to be loaded.
 *     @type string $thumb_url The teaser image of the video, to placehold the video.
 *     @type string $title The title of the video (Displayed on Pause).
 *     @type string $subtitle The subtitle of the video (Displayed on Pause).
 *     @type string $width A hardcoded width for the video.
 *     @type string $height A hardcoded height for the video.
 * }
 * @param string $content Contains the content of the shortcode.
 */
function ir_embedded_video($params) {
	global $add_videoplayer_scripts;
	$add_videoplayer_scripts = true;
	
	return ir_render_embedded_video($params);
}
add_shortcode('embed-ir-video', 'Roots\Sage\Shortcodes\ir_embedded_video');

/**
 * Shortcode - Displays an award.
 *			
 * A shortcode to display an award section.
 * 
 * @since 1.0.0
 *
 * @param array $params {
 *     @type int $id The id of the award to be displayed.
 *     @type string $type The type of award, some appends required addition information.
 * }
 */
function ir_display_award($params) {
	return ir_render_display_award($params);
}
add_shortcode('display-award', 'Roots\Sage\Shortcodes\ir_display_award');

/**
 * Shortcode - Displays the VR Hover Javascript Module.
 *			
 * A shortcode to display the VR Hover Javascript Module. On mouse hover, depending on position it displays a different image
 * there are 5 images: center, left, right, bottom, top
 * 
 * @since 1.0.0
 * 
 */
function ir_vr_image_hover() {
	return ir_render_vr_image_hover();
}
add_shortcode('display-vr-hover', 'Roots\Sage\Shortcodes\ir_vr_image_hover');

/**
 * Shortcode - Displays an IR-button Styled.
 *			
 * A shortcode to display an IR-button Styled
 * 
 * @since 1.0.0
 *
 * @param array $params {
 *     @type string $title The title of the button, to be displayed inside the button.
 *     @type string $color The color scheme of the button (black || white).
 *     @type string $class CSS classes to append to the element.
 *     @type string $href The url the that button will link too.
 * }
 * @param string $content Contains the content of the shortcode.
 */
function ir_button($params) {
	return ir_render_button($params);
}
add_shortcode('ir-button', 'Roots\Sage\Shortcodes\ir_button');

/**
 * Shortcode - Displays the Converse chucks 4 images and hover state.
 *			
 * A shortcode to display the Converse chucks 4 images and hover state. There are 4 images,
 * with a hover state showing their title and subtitle.
 * 
 * @since 1.0.0
 *
 * @param array $params {
 *     @type type $var Description.
 *     @type type $var Description.
 * }
 * @param string $content Contains the content of the shortcode.
 */
function ir_converse_chucks($params) {
	return ir_render_converse_chucks($params);
}
add_shortcode('ir-converse-chucks', 'Roots\Sage\Shortcodes\ir_converse_chucks');

/**
 * Shortcode - Displays a Collaboration with Tool section.
 *			
 * A shortcode to display a Collaboration with Tool section. Typically used in Project Work Pages.
 * 
 * @since 1.0.0
 *
 * @param array $params {
 *     @type type $var Description.
 *     @type type $var Description.
 * }
 */
function ir_collaboration_tool($params) {
	return ir_render_collaboration_tool($params);
}
add_shortcode('tool-collaboration', 'Roots\Sage\Shortcodes\ir_collaboration_tool');

/**
 * Shortcode - Displays a basic IR Section.
 *			
 * A shortcode to display the Clients Section on the Work Page. Loads all of the Clients Post type.
 *
 * @param array $params {
 *     @type string $title Should display clients on front page?
 * 	   @type string $container_class the class to be appended to the container.
 * }
 * @param string $content Contains the content of the shortcode.
 * 
 * @since 1.0.0
 */
function ir_ir_section($params, $content) {
	return ir_render_ir_section($params, $content);
}
add_shortcode('ir-section', 'Roots\Sage\Shortcodes\ir_ir_section');

/**
 * Shortcode - Displays the Leadership section of About Us.
 *			
 * A shortcode to display the Leadership section of About Us. Appends a <h2>Leadership</h2> to the top.
 *		
 * @since 1.0.0
 *
 * @param array $params {
 *     @type string $class CSS classes that should append to this html item.
 * }
 * @param string $content Contains the content of the shortcode.
 */
function ir_leadership($params, $content) {
	return ir_render_leadership($params, $content);
}
add_shortcode('ir-leadership', 'Roots\Sage\Shortcodes\ir_leadership');

/**
 * Shortcode - Displays the the preview of Idea Rebel Services page..
 *			
 * A shortcode to display the Display the preview of the services page. Takes an input array and prints out the Services preview section.
 * 
 * @since 1.0.0
 *
 * @param array $params {
 *     @type string $services A string array which is delimited by a comma. Ex. Strategy, Marketing, Creative, etc.
 * }
 * @param string $content Contains the content of the shortcode.
 */
function ir_services_preview($params) {
	return ir_render_services_preview($params);
}
add_shortcode('ir-services-preview', 'Roots\Sage\Shortcodes\ir_services_preview');