<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/** 
 * Add metaboxes for the individual page fields 
 * 
 * @since 1.0.0
 */ 
function add_my_afc_metabox() {
	//They only get added on the underground...
    pods_group_add( 'page', 'Header Hero Carousel', 'header_image_widescreen,header_image_desktop,header_image_tablet,header_image_mobile,nav_menu_color,header_title,header_subtitle,header_display_type,header_background_color' );

}
add_action( 'pods_meta_groups', __NAMESPACE__ . '\\add_my_afc_metabox');

/**
 * Display the Offices in the footer
 *			
 * Generates the offices based off of the available Offices Post type. Maximum 4.
 * 
 * @since 1.0.0
 */
function display_offices_footer() {
	 
	$args = array(
	  'post_type' => 'office',
	  'post_status' => 'publish',
	  'orderby' => 'date',
	  'order' => 'ASC'
	);
	
	$office_query = new \WP_Query($args);
	if($office_query->have_posts()) {
		while ($office_query->have_posts() ) {
			$office_query->the_post();
			$timezone = get_post_meta(get_the_ID() , 'timezone', true);
			if(!empty($timezone)) {
				$datetime = date_format(new \DateTime("now", new \DateTimeZone($timezone)), 'g:i a');
			}
			echo '<div class="footer-office-item col-xs-12 col-sm-4 col-md-4">' 
					. '<div class="footer-office-item-inner box">'
						. '<a class="footer-office-anchor" href="' . get_permalink(get_the_ID()) .'"><div class="box-content">'
							. '<div class="footer-office-edge-text footer-office-phone row">' . str_replace("-", " ", get_post_meta(get_the_ID() , 'phone_number', true)) . '</div>'
							. '<div class="footer-office-edge-text footer-office-city row"><span class="ir-footer-office-item ir-hover-span-underline">' . get_post_meta(get_the_ID() , 'city_abrev', true) . '</span></div>'
							. '<div class="footer-office-edge-text footer-office-address row">' . get_post_meta(get_the_ID() , 'email_address', true) . '</div>'
							. '<div class="footer-office-edge-text footer-office-time row">' .  $datetime . '</div>'
							. '<div class="footer-office-edge-text footer-office-weather row">' . get_post_meta(get_the_ID() , 'temperature', true) ." &#8451;"  . '</div>'
						. '</div></a>'
					. '</div>'
				. '</div>';
		}
	}

	wp_reset_postdata();
}


/**
 * Display a random call phrase with number in the backend.
 *			
 * The phrase is set as a taxonomy under Offices.
 * Footer call number set as a taxonomy under Offices
 * 
 * @since 1.0.0
 */
 function display_rand_call_prompt() {
	//Get Random Phrase
	$call_phrases_pod = pods('callphrase');
	$call_phrases_pod->find('call_phrase ASC');
	$phrase_count = $call_phrases_pod->total();
	$rand_num = rand(0, $phrase_count-1);
	$count = 0;
	while ( $call_phrases_pod->fetch() ) {
		$call_prefix = $call_phrases_pod->field('call_phrase');
		if($count == $rand_num) {
			break;
		}
		$count++;
	}
	
	//Get Footer Number
	$call_number_pod = pods('footercallnumber');
	$call_number_pod->find('footer_call_number ASC');
	
	if ($call_number_pod->fetch() ) {
	    $call_number = $call_number_pod->field('footer_call_number');
	}
	if($call_prefix && $call_number) {
		echo "$call_prefix <span class='visible-xs' style='height:0;'><br/></span>$call_number";
	}
 }

/**
 * Display the Header Hero Image / Carousel.
 *			
 * If Homepage it is a carousel. Any other page is currently just a hero image and title.
 * 
 * @since 1.0.0
 * 
 * @param int $post_id The id of the post being displayed in the carousel or the page. Used as a link in the header for the down arrow. 
 */
function display_ir_carousel($post_id) {
	//Load the Homepage Carousel.
	if(is_front_page()) {
		$carousel_postion = 0;
		//About Page Info
		$args = array(
			'meta_key' => '_wp_page_template',
			'meta_value' => 'template-home2.php',
			'post_type' => 'page',
			'post_status' => 'publish',
			'limit' => 1
		);
	
		$home_page = get_pages($args);
		if(!empty($home_page)) {
			$home_page_pods = pods('page', $home_page[0]->ID);
			add_carousel_item($home_page_pods->field('header_title'), $home_page_pods->field('header_subtitle'), 
			$home_page_pods->field('nav_menu_color'), 
			$carousel_postion++, $post_id, $home_page_pods->display('header_image_widescreen'), 
			$home_page_pods->display('header_image_desktop'), $home_page_pods->display('header_image_tablet'),
			$home_page_pods->display('header_image_mobile'), $home_page_pods->display('header_display_type'), $home_page_pods->display('header_background_color'));
		}
		
		//Project Sliders
		global $slider_query;
		$args = array('post_type' => 'workproject',
						'orderby' => 'meta_value_num',
						'meta_key'  => 'display_order',
						'order' => 'ASC');
		$slider_query = new \WP_Query($args);
		while($slider_query->have_posts()) {
			$slider_query->the_post();
			$slider_video_wide = get_post_meta(get_the_ID() , 'header_video_widescreen', true);
			$slider_image_wide = get_post_meta(get_the_ID(), 'header_image_widescreen', true);
			$slider_image_desk = get_post_meta(get_the_ID(), 'header_image_desktop', true);
			$slider_image_tab = get_post_meta(get_the_ID(), 'header_image_tablet', true);
			$slider_image_mob = get_post_meta(get_the_ID(), 'header_image_mobile', true);
			$slider_display_type =  get_post_meta(get_the_ID(), 'header_display_type', true);
			$slider_background_color = get_post_meta(get_the_ID(), 'header_background_color', true);
			if(!empty($slider_video_wide)) {
				$slider_video_wide = $slider_video_wide['guid'];
			}
			if(!empty($slider_image_wide)) {
				$slider_image_wide = $slider_image_wide['guid'];
			}
			if(!empty($slider_image_desk)) {
				$slider_image_desk = $slider_image_desk['guid'];
			}
			if(!empty($slider_image_tab)) {
				$slider_image_tab = $slider_image_tab['guid'];
			}
			if(!empty($slider_image_mob)) {
				$slider_image_mob = $slider_image_mob['guid'];
			}
			if(!empty($slider_video_wide)) {
				add_carousel_video(get_post_meta(get_the_ID(), 'header_title', true), get_post_meta(get_the_ID(), 'header_subtitle', true),
				get_post_meta(get_the_ID(), 'nav_menu_color', true), $carousel_postion++, $post_id, $slider_video_wide, $slider_image_wide);
			} else {
				add_carousel_item(get_post_meta(get_the_ID(), 'header_title', true), get_post_meta(get_the_ID(), 'header_subtitle', true),
				get_post_meta(get_the_ID(), 'nav_menu_color', true), $carousel_postion++, $post_id, $slider_image_wide, $slider_image_desk, $slider_image_tab,
				$slider_image_mob, $slider_display_type, $slider_background_color);
			}
			
		}
		
		//Culture Sliders
		global $slider_query;
		$args = array('post_type' => 'home_culture',
						'orderby' => 'meta_value_num',
						'meta_key'  => 'display_order',
						'order' => 'ASC');
		$slider_query = new \WP_Query($args);
		while($slider_query->have_posts()) {
			$slider_query->the_post();
			$slider_video_wide = get_post_meta(get_the_ID() , 'culture_video_widescreen', true);
			$slider_image_desk = get_post_meta(get_the_ID(), 'culture_image_desktop', true);
			$slider_image_mob = get_post_meta(get_the_ID(), 'culture_image_mobile_retina', true);
			$slider_background_color = get_post_meta(get_the_ID(), 'home_background_color', true);
			if(!empty($slider_video_wide)) {
				$slider_video_wide = $slider_video_wide['guid'];
			}
			if(!empty($slider_image_desk)) {
				$slider_image_desk = $slider_image_desk['guid'];
			}
			if(!empty($slider_image_mob)) {
				$slider_image_mob = $slider_image_mob['guid'];
			}
			if(!empty($slider_video_wide)) {
				add_carousel_video(get_post_meta(get_the_ID(), 'header_title', true), get_post_meta(get_the_ID(), 'home_excerpt', true),
				get_post_meta(get_the_ID(), 'nav_menu_color', true), $carousel_postion++, $post_id, $slider_video_wide);
			} else {
				add_carousel_item(get_post_meta(get_the_ID(), 'header_title', true), get_post_meta(get_the_ID(), 'home_excerpt', true),
				get_post_meta(get_the_ID(), 'nav_menu_color', true), $carousel_postion++, $post_id, $slider_image_desk,
				$slider_image_mob, $slider_background_color);
			}
			
		}
	} else {
		global $post;

		if($post->post_type == 'jobpost'){
			
			$terms = get_the_terms($post->ID , 'jobtype');
			// call your taxonomy as a pod because when you extend it, it makes it a pod object
			$job_type = pods( 'jobtype' );
			//do first term and get the image fields
			$job_type->fetch( $terms[0]->term_id );

			$project_title = get_post_meta($post->ID , 'job_title', true);
			$project_subtitle = get_post_meta($post->ID , 'job_secondary_title', true);

			$header_image_wide = $job_type->field('header_image_widescreen');
			$header_image_desk = $job_type->field('header_image_desktop');
			$header_image_tab = $job_type->field('header_image_tablet');
			$header_image_mob = $job_type->field('header_image_mobile');
			$header_display_type = $job_type->field('header_display_type');
			$header_background_color = $job_type->field('header_background_color');
		}
		else if($post->post_type == 'office'){

			$project_title = get_post_meta($post->ID , 'header_title', true);
			$project_subtitle  = '';
			$header_image_wide = get_post_meta(get_the_ID() , 'header_image_widescreen', true);
			$header_image_desk = get_post_meta(get_the_ID() , 'header_image_desktop', true);
			$header_image_tab = get_post_meta(get_the_ID() , 'header_image_tablet', true);
			$header_image_mob = get_post_meta(get_the_ID() , 'header_image_mobile', true);
			$header_display_type = get_post_meta(get_the_ID() , 'header_display_type', true);
			$header_background_color = get_post_meta(get_the_ID() , 'header_background_color', true);
		}
		else{
			$project_title = get_post_meta(get_the_ID() , 'header_title', true);
			$project_subtitle = get_post_meta(get_the_ID() , 'header_subtitle', true);
			$header_video_wide = get_post_meta(get_the_ID() , 'header_video_widescreen', true);
			$header_image_wide = get_post_meta(get_the_ID() , 'header_image_widescreen', true);
			$header_image_desk = get_post_meta(get_the_ID() , 'header_image_desktop', true);
			$header_image_tab = get_post_meta(get_the_ID() , 'header_image_tablet', true);
			$header_image_mob = get_post_meta(get_the_ID() , 'header_image_mobile', true);
			$header_display_type = get_post_meta(get_the_ID() , 'header_display_type', true);
			$header_background_color = get_post_meta(get_the_ID() , 'header_background_color', true);
		}
		if(!empty($header_video_wide)) {
			$header_video_wide = $header_video_wide['guid'];
		}
		if(!empty($header_image_wide)) {
			$header_image_wide = $header_image_wide['guid'];
		}
		if(!empty($header_image_desk)) {
			$header_image_desk = $header_image_desk['guid'];
		}
		if(!empty($header_image_tab)) {
			$header_image_tab = $header_image_tab['guid'];
		}
		if(!empty($header_image_mob)) {
			$header_image_mob = $header_image_mob['guid'];
		}
		$nav_color = get_post_meta(get_the_ID() , 'nav_menu_color', true);
		if(!empty($header_image)) {
			$header_image = $header_image['guid'];
		} else {
			$header_image = "";
		}
		if(!empty($header_video_wide)) {
			add_carousel_video($project_title, $project_subtitle, $nav_color, 0, $post_id, $header_video_wide, $header_image_wide);
		} else {
			add_carousel_item($project_title, $project_subtitle, $nav_color, 0, $post_id,
								$header_image_wide, $header_image_desk, $header_image_tab, $header_image_mob, $header_display_type, $header_background_color);
		}
	}
}

/**
 * Add an individual item to the header carousel.
 *			
 * Primarily used for homepage.
 * 
 * @since 1.0.0
 * 
 * @param string $title The title of this carousel item.
 * @param string $subtitle The subtitle of this carousel item.
 * @param string $nav_color The color scheme of this carousel item (black || white).
 * @param string $header_image The url of the image to be used in the header or carousel.
 * @param string $position The position of the item in the carousel
 * @param int $post_id The id of the post being displayed in the carousel or the page. Used as a link in the header for the down arrow. 
 * @param boolean $is_home A flag to determine if is HomePage or not
 * @param string $type The type of carousel item is full | center
 * @param string $background_color Background color for the carousel image.
 */		
function add_carousel_item($title, $subtitle, $nav_color, $position, $post_id,
							$header_image_wide = null, $header_image_desk = null, $header_image_tab = null, $header_image_mob = null,
							$type = "Full", $background_color = "initial") {
	$is_home = is_front_page();
	echo "<div id='slick-carousel-position-" . $position . "' class='header-ir-image-container " . (($nav_color == "White") ? 'append-nav-white' : 'append-nav-black') . " " . (($type === 'Center') ? 'header-ir-center' : '' ) . "' style='background-color:$background_color;'>";
		echo add_responsive_image_element(array(
											"wide_url" => $header_image_wide,
											"desk_url" => $header_image_desk,
											"tab_url" => $header_image_tab,
											"mob_url" => $header_image_mob
											), 'Header', 'header-ir-image');
		echo "<div class='header-ir-titlebox " . (($is_home) ? 'header-ir-titlebox-home' : "") ."'>";
			if($position == 0) {
				echo "<h1>$title</h1>";
			} else {
				echo "<h2>$title</h2>";
			}
			echo "<div class='header-ir-subtitle'>$subtitle</div>";
		echo "</div>";
		echo "<div class='header-ir-select-button-container'>";
			echo "<a class='nav-scrolldown-button' id='navdown-" . $post_id . "'><img class='header-ir-select-button' src='" . \get_template_directory_uri() . "/assets/images/header-ir-select-button-" . (($nav_color == "White") ? 'white' : 'black') . ".png' alt='Nav' /></a>";
		echo "</div>";
	echo "</div>";
}
 
/**
 * Add an individual video to the header carousel.
 *			
 * Primarily used for homepage.
 * 
 * @since 1.0.0
 * 
 * @param string $title The title of this carousel item.
 * @param string $subtitle The subtitle of this carousel item.
 * @param string $nav_color The color scheme of this carousel item (black || white).
 * @param string $header_video The url of the video to be used in the header or carousel.
 * @param string $position The position of the item in the carousel
 * @param int $post_id The id of the post being displayed in the carousel or the page. Used as a link in the header for the down arrow. 
 * @param boolean $is_home A flag to determine if is HomePage or not
 */		
function add_carousel_video($title, $subtitle, $nav_color, $position, $post_id, $header_video, $header_thumb) {
	$is_home = is_front_page();
	echo "<div id='slick-carousel-position-" . $position . "' class='header-ir-image-container " . (($nav_color == "White") ? 'append-nav-white' : 'append-nav-black') . "'>";
		echo do_shortcode('[embed-ir-video thumb_url="' . $header_thumb . '" video_url="' . $header_video . '" id="header-video" class="ir-header-video" is_header=true ' . (($is_home) ? ' front_page=true': '').' ]');
		echo "<div class='header-ir-titlebox " . (($is_home) ? 'header-ir-titlebox-home' : "") ."'>";
			if($position == 0) {
				echo "<h1>$title</h1>";
			} else {
				echo "<h2>$title</h2>";
			}
			echo "<div class='header-ir-subtitle'>$subtitle</div>";
		echo "</div>";
		//TODO: add JS on click slide animation.
		echo "<div class='header-ir-select-button-container'>";
			echo "<a class='nav-scrolldown-button' id='navdown-" . $post_id . "'><img class='header-ir-select-button' src='" . \get_template_directory_uri() . "/assets/images/header-ir-select-button-" . (($nav_color == "White") ? 'white' : 'black') . ".png' alt='Nav' /></a>";
		echo "</div>";
	echo "</div>";
}

/** Print out all of the projects on the work page.
 * 
 * Prints out the DOM elements for each Project represented on the Work Page.
 * 
 * @since 1.0.0
 * 
 */ 
function display_work_projects() {
	$args = array('post_type' => 'workproject',
					'orderby' => 'meta_value_num',
					'meta_key'  => 'display_order',
					'order' => 'ASC');
	$slider_query = new \WP_Query($args);
	while($slider_query->have_posts()) {
		$slider_query->the_post();
		$slider_image_wide = get_post_meta(get_the_ID(), 'thumb_image_widescreen', true);
		$slider_image_desk = get_post_meta(get_the_ID(), 'thumb_image_desktop', true);
		$slider_image_tab = get_post_meta(get_the_ID(), 'thumb_image_tablet', true);
		$slider_image_mob = get_post_meta(get_the_ID(), 'thumb_image_mobile', true);
		if(!empty($slider_image_wide)) {
			$slider_image_wide = $slider_image_wide['guid'];
		}
		if(!empty($slider_image_desk)) {
			$slider_image_desk = $slider_image_desk['guid'];
		}
		if(!empty($slider_image_tab)) {
			$slider_image_tab = $slider_image_tab['guid'];
		}
		if(!empty($slider_image_mob)) {
			$slider_image_mob = $slider_image_mob['guid'];
		}
		display_work_project_section(get_post_meta(get_the_ID(), 'header_title', true),
			get_post_meta(get_the_ID(), 'header_subtitle', true),
			get_permalink(get_the_ID()), $slider_image_wide, $slider_image_desk, $slider_image_tab, $slider_image_mob);
	}
	
	wp_reset_postdata();
}

/** Print out all of the culture posts on the home page.
 * 
 * Prints out the DOM elements for each Project represented on the Work Page.
 * 
 * @since 1.0.0
 * 
 */ 

/* Home culture section */
function display_home_culture_assets() {
	$args = array('post_type' => 'home_culture',
				    'posts_per_page' => '1',
					'orderby' => 'rand');
	$slider_query = new \WP_Query($args);
	while($slider_query->have_posts()) {
		$slider_query->the_post();
		$slider_image_desk = get_post_meta(get_the_ID(), 'culture_image_desktop', true);
		$slider_image_mob = get_post_meta(get_the_ID(), 'culture_image_mobile_retina', true);
		if(!empty($slider_image_desk)) {
			$slider_image_desk = $slider_image_desk['guid'];
		}
		if(!empty($slider_image_mob)) {
			$slider_image_mob = $slider_image_mob['guid'];
		}
		display_home_culture(get_post_meta(get_the_ID(), 'home_title', true),
			get_post_meta(get_the_ID(), 'home_background_color', true),
			get_post_meta(get_the_ID(), 'home_excerpt', true),
			get_post_meta(get_the_ID(), 'culture_image_desktop', true),
			get_permalink(get_the_ID()), $slider_image_desk, $slider_image_mob);
	}
	wp_reset_postdata();
}

/** Add work items to be dispalyed to the home page.
 * 
 * Prints out the DOM elements for a Project represented on the Work Page.
 * 
 * @since 1.0.0
 * 
 * @param string $title The title of this work section.
 * @param string $subtitle The subtitle of this work section.
 * @param string $excerpt The excerpt of this work section.
 * @param string $header_image The url of the image to be used in this work section.
 * @param string $link The url that this work section will lead to.
 * 
 */
	// echo "<div id='home_workContainer' class='scrollContainer'>";
	$home_cultureSec = 0;
function display_home_culture($title, $excerpt, $link, $image_desk, $image_mob) {
	echo "<div id='display_home_culture' class='home_culture-section_"; echo $home_cultureSec++; echo "'><div class='home_culture-section'>";
		echo add_responsive__css_bg_image_element(array(
											"desk_url" => $image_desk,
											"mob_url" => $image_mob
											), 'HomeCultureItem', 'home_culture-section-img');
		echo "<div class='home_culture-section-titlebox'>";
			echo "<h3>$title</h3>";
			echo "<div class='home_culture-section-excerpt'>$excerpt</div>";
		echo "</div>";
	echo "</div>";
}

/** Print out all of the projects on the home page.
 * 
 * Prints out the DOM elements for each Project represented on the Work Page.
 * 
 * @since 1.0.0
 * 
 */ 

/* 3 Work posts in scrolling container */
function display_work_projects_onHome() {
	$args = array('post_type' => 'workproject',
					'orderby' => 'rand');
	$slider_query = new \WP_Query($args);
	while($slider_query->have_posts()) {
		$slider_query->the_post();
		$slider_image_wide = get_post_meta(get_the_ID(), 'thumb_image_widescreen', true);
		$slider_image_desk = get_post_meta(get_the_ID(), 'thumb_image_desktop', true);
		$slider_image_tab = get_post_meta(get_the_ID(), 'thumb_image_tablet', true);
		$slider_image_mob = get_post_meta(get_the_ID(), 'thumb_image_mobile', true);
		$slider_url = get_permalink();
		if(!empty($slider_image_wide)) {
			$slider_image_wide = $slider_image_wide['guid'];
		}
		if(!empty($slider_image_desk)) {
			$slider_image_desk = $slider_image_desk['guid'];
		}
		if(!empty($slider_image_tab)) {
			$slider_image_tab = $slider_image_tab['guid'];
		}
		if(!empty($slider_image_mob)) {
			$slider_image_mob = $slider_image_mob['guid'];
		}
		display_work_project_section_onHome(get_post_meta(get_the_ID(), 'header_title', true),
			get_post_meta(get_the_ID(), 'header_subtitle', true),
			get_post_meta(get_the_ID(), 'home_excerpt', true),
			get_permalink(get_the_ID()), $slider_image_wide, $slider_image_desk, $slider_image_tab, $slider_image_mob);
	}
	
	wp_reset_postdata();
}

/** Add work items to be dispalyed to the home page.
 * 
 * Prints out the DOM elements for a Project represented on the Home Page.
 * 
 * @since 1.0.0
 * 
 * @param string $title The title of this work section.
 * @param string $subtitle The subtitle of this work section.
 * @param string $excerpt The excerpt of this work section.
 * @param string $header_image The url of the image to be used in this work section.
 * @param string $link The url that this work section will lead to.
 * 
 */
function display_work_project_section_onHome($title, $subtitle, $excerpt, $link, $image_wide, $image_desk, $image_tab, $image_mob) {
	
	echo "<div class='section fp-auto-height'><div class='ir-work-section'>";
		echo add_responsive_image_element(array(
											"wide_url" => $image_wide,
											"desk_url" => $image_desk,
											"tab_url" => $image_tab,
											"mob_url" => $image_mob
											), 'WorkItem', 'ir-work-section-img');
		echo "<div class='ir_workTitle'><div class='ir-work-section-titlebox'>";
			echo "<span class='ir_workTitle_featured'>FEATURED WORK</span>";
			echo "<h3><a href='$link'>$title</a></h3>";
			echo "<div class='ir-work-section-subtitle'>$subtitle</div>";
			echo "<div class='ir-work-section-excerpt'>$excerpt</div>";
		echo "</div></div>";
	echo "</div></div>";
}

/** Add item to be dispalyed to the work page.
 * 
 * Prints out the DOM elements for a Project represented on the Work Page.
 * 
 * @since 1.0.0
 * 
 * @param string $title The title of this work section.
 * @param string $subtitle The subtitle of this work section.
 * @param string $header_image The url of the image to be used in this work section.
 * @param string $link The url that this work section will lead to.
 * 
 */
function display_work_project_section($title, $subtitle, $link, $image_wide, $image_desk, $image_tab, $image_mob) {
	echo "<div class='col-sm-6 ir-work-section'><a class='ir-work-section-link' href='$link'><div class='ir-work-section col-xs-12'>";
		echo add_responsive_image_element(array(
											"wide_url" => $image_wide,
											"desk_url" => $image_desk,
											"tab_url" => $image_tab,
											"mob_url" => $image_mob
											), 'WorkItem', 'ir-work-section-img');
		echo "<div class='ir_workTitle'><div class='ir-work-section-titlebox'>";
			echo "<h3>$title</h3>";
			echo "<div class='ir-work-section-subtitle'>$subtitle</div>";
		echo "</div></div>";
	echo "</div></a></div>";
}

/** Add the project navigation to the bottom of the project page. Previous -- Next
 * 
 * Echo out the project navigation bar to navigation previous / next in projects
 * Display's project navigation at the bottom of the homepage and the project details page.
 * 
 * @since 1.0.0
 * 
 */
function display_project_navigation() {
	$args = array('post_type' => 'workproject',
					'orderby' => 'meta_value_num',
					'meta_key'  => 'display_order',
					'order' => 'ASC',
				    'nopaging'        => true,
				    'posts_per_page'  => 100,
				    'limit'           => 100);
	$postlist = get_posts($args);
	
	$posts = array();
	$post_count = 0;
	foreach ($postlist as $post) {
	    $posts[] += $post->ID;
		$post_count++;
	}
	
	$current = array_search(get_the_ID(), $posts);
	$prevCount = $current-1;
	$nextCount = $current+1;
	$prevCount = (($prevCount < 0) ? ($post_count-1) : $prevCount);
	$nextCount = (($nextCount >= $post_count) ? 0 : $nextCount);
	$prevID = $posts[$prevCount];
	$nextID = $posts[$nextCount];
	echo "<div class='ir-project-nav'>";
	if (!empty($prevID)) { 
		echo "<div class='ir-project-nav-prev col-xs-6'>";
			echo"<div class='ir-project-nav-container'><span class='ir-center-helper'></span>";
			echo "<a class='ir-project-nav-button ir-project-nav-prev-button " .((is_front_page()) ? 'ir-home-nav-prev' : '') ."' href='"
			.  ((is_front_page()) ? '' : get_permalink($prevID)) . "'  title='" . get_the_title($prevID) . "'>";
			echo "<div class='ir-project-nav-text'>Previous Project</div>";
			echo "<br/>";
			echo "<i class='fa fa-long-arrow-left'></i> <span class='ir-project-nav-span ir-hover-span-underline'><h8>" . get_post_meta($prevID, 'header_title', true) . "</h8></span></a>";
			echo "</div>";
		echo "</div>";
	}
	if (!empty($nextID)) { 
		echo "<div class='ir-project-nav-next col-xs-6'>";
			echo"<div class='ir-project-nav-container'><span class='ir-center-helper'></span>";
			echo "<a class='ir-project-nav-button ir-project-nav-next-button " .((is_front_page()) ? 'ir-home-nav-next' : '') ."' href='"
			.  ((is_front_page()) ? '' : get_permalink($nextID)) . "'  title='" .  get_the_title($nextID) . "'>";
			echo "<div class='ir-project-nav-text'>Next Project</div>";
			echo "<br/>";
			echo "<span class='ir-project-nav-span ir-hover-span-underline'><h8>" . get_post_meta($nextID, 'header_title', true) ."</h8></span> <i class='fa fa-long-arrow-right'></i></a>";
			echo "</div>";
		echo "</div>";
	}
	echo "</div>";
}

/** Add Image <picture> with Responsive Breakpoints
 * 
 * Returns a set of DOM elements with responsive breakpoints and other images
 * For Retina images it sets widescreen image for desktop.
 * 
 * @since 1.0.0
 * 
 * @param dictionary $urls A dictionary of screen size urls $wide_url, $wide_url_retina, $desk_url, $desk_url_retina, $tab_url, $tab_url_retina, $mob_url, $mob_url_retina.
 * @param string $alt The name of the image, to put in the img alt tag.
 * @param string $mobile_url The url for the mobile breakpoint image.
 * @param string $class_append Additional classes to add to the picture element.
 * 
 */

function add_responsive_image_element($urls, $alt, $class_append) {
	//TODO: add SVGs
	$img_urls = array(
		"wide_url" => (!empty($urls['wide_url']) ? $urls['wide_url'] : ""),
		"wide_url_retina" => (!empty($urls['wide_url_retina']) ? $urls['wide_url_retina'] : ""),
		"desk_url" => (!empty($urls['desk_url']) ? $urls['desk_url'] : ""),
		"desk_url_retina" => (!empty($urls['desk_url_retina']) ? $urls['desk_url_retina'] : $urls['wide_url']),
		"tab_url" => (!empty($urls['tab_url']) ? $urls['tab_url'] : ""),
		"tab_url_retina" => (!empty($urls['tab_url_retina']) ? $urls['tab_url_retina'] : ""),
		"mob_url" => (!empty($urls['mob_url']) ? $urls['mob_url'] : ""),
		"mob_url_retina" => (!empty($urls['mob_url_retina']) ? $urls['mob_url_retina'] : ""),
	);
	
	
	$widescreen_url = $img_urls['wide_url'] . ((!empty($img_urls['wide_url_retina'])) ? (", " . $img_urls['wide_url_retina'] . " 2x") : "" );
	$desktop_url = $img_urls['desk_url'] . ((!empty($img_urls['desk_url_retina'])) ? (", " . $img_urls['desk_url_retina'] . " 2x") : "" );
	$tablet_url = $img_urls['tab_url'] . ((!empty($img_urls['tab_url_retina'])) ? (", " . $img_urls['tab_url_retina'] . " 2x") : "" );
	$mobile_url = $img_urls['mob_url'] . ((!empty($img_urls['mob_url_retina'])) ? (", " . $img_urls['mob_url_retina'] . " 2x") : "" );
	
	if(empty($widescreen_url)) {
		return '';
	}
	if(empty($desktop_url)) {
		$desktop_url = $widescreen_url;
	}
	if(empty($tablet_url)) {
		$tablet_url = $desktop_url;
	}
	if(empty($mobile_url)) {
		$mobile_url = $tablet_url;
	}
	
	$desk_break = 1920;
	$tab_break = 1024;
	$mob_break = 768;
	$responsive_image = '<picture>';
    $responsive_image .= '<source media="screen and (min-width: ' . ($tab_break) . 'px)" srcset="' . $desktop_url . '" />';
    $responsive_image .= '<source media="screen and (min-width: ' . $mob_break . 'px) and (max-width: ' . ($tab_break-1) . 'px)" srcset="' . $tablet_url . '" />';
    $responsive_image .= '<source media="screen and (max-width: ' . ($mob_break - 1) . 'px)" srcset="' . $mobile_url . '" />';
    $responsive_image .= '<img class="ir-image ' . $class_append . '" src="' . $img_urls['wide_url'] . '" alt="' . $alt . '" />';
	$responsive_image .= '</picture>';

	return $responsive_image;
	
}

/** Add Image <picture> with Responsive Breakpoints *** Added for background-image css centering
 * 
 * Returns a set of DOM elements with responsive breakpoints and other images
 * For Retina images it sets widescreen image for desktop.
 * 
 * @since 1.0.0
 * 
 * @param dictionary $urls A dictionary of screen size urls $wide_url, $wide_url_retina, $desk_url, $desk_url_retina, $tab_url, $tab_url_retina, $mob_url, $mob_url_retina.
 * @param string $alt The name of the image, to put in the img alt tag.
 * @param string $mobile_url The url for the mobile breakpoint image.
 * @param string $class_append Additional classes to add to the picture element.
 * 
 */

function add_responsive__css_bg_image_element($urls, $alt, $class_append) {
	//TODO: add SVGs
	$img_urls = array(
		"desk_url" => (!empty($urls['desk_url']) ? $urls['desk_url'] : ""),
		"mob_url" => (!empty($urls['mob_url']) ? $urls['mob_url'] : ""),
	);
	
	
	$desktop_url = $img_urls['desk_url'];
	$mobile_url = $img_urls['mob_url'];
	
	
	$desk_break = 1920;
	$mob_break = 768;
	$responsive_image = '<picture>';
    $responsive_image .= '<source srcset="' . $desktop_url . '" />';
    $responsive_image .= '<img class="ir-image ' . $class_append . '" src="' . $img_urls['desk_url'] . '" alt="' . $alt . '" />';
	$responsive_image .= '</picture>';

	return $responsive_image;
	
}

///temperature stuff for the offices
// Add function to register event to WordPress init
add_action( 'wp', __NAMESPACE__ . '\\register_temperature_update');

// Function which will register the event
function register_temperature_update() {
	// Make sure this event hasn't been scheduled
	if( !wp_next_scheduled('update_office_weather' ) ) {
		// Schedule the event
		wp_schedule_event( time(), 'hourly', 'update_office_weather' );
	}
}

// delete_post_revisions will be call when the Cron is executed
add_action( 'update_office_weather', __NAMESPACE__ . '\\update_office_temp' );

//update the temperatures of the offices every hour
function update_office_temp() {
	$apiKey = '55839af703ea3325';

	$params = array('limit' => -1);
	$pod = pods('office', $params);

	while ( $pod->fetch() ) {
	  $city = str_replace(' ', '_', $pod->field('city'));
	  
	  if(strtolower($pod->field('country')) == 'canada'){
	  	$location = $pod->field('country') . '/' . $city;
	  }
	  else{
	  	$provincestate = $pod->field('provincestate');
		$location = $provincestate . '/' . $city;
	  }

	  $weather = get_temperature_celcius($location, $apiKey);

	  $pod->save('temperature', $weather['temp']);
  	  $pod->save('current_weather_conditions', $weather['weather_conditions']);
  	  $pod->save('sunset', $weather['sunset']);
  	  $pod->save('sunrise', $weather['sunrise']);
	  
	}
}
/**
 * gets the weather from wunderground
 * @param String $location 
 * @param String $apiKey 
 * @return String of the temperature in celcius ie 20, 13.6 etc
 * add /astronomy to get sunset
 * example us http://api.wunderground.com/api/55839af703ea3325/forecast/geolookup/conditions/q/CA/San_Francisco.json
 * example  http://api.wunderground.com/api/55839af703ea3325/geolookup/conditions/forecast/astronomy/q/Australia/Sydney.json
 */
function get_temperature_celcius($location, $apiKey){
  $json_string = file_get_contents("http://api.wunderground.com/api/". $apiKey ."/geolookup/conditions/astronomy/q/". $location .".json");
  //$json_string = file_get_contents('weather.json', FILE_USE_INCLUDE_PATH);
  $parsed_json = json_decode($json_string);

  $temp_c = $parsed_json->{'current_observation'}->{'temp_c'};
  $conditions = $parsed_json->{'current_observation'}->{'weather'};
  $sunrise = $parsed_json->{'sun_phase'}->{'sunrise'}->{'hour'} . ':' . $parsed_json->{'sun_phase'}->{'sunrise'}->{'minute'};
  $sunset = $parsed_json->{'sun_phase'}->{'sunset'}->{'hour'} . ':' . $parsed_json->{'sun_phase'}->{'sunset'}->{'minute'};
  
  $weather_conditions_map = get_weather_conditions_map();
  $conditions = $weather_conditions_map[$conditions];
  
  return array('temp' => $temp_c, 'weather_conditions' => $conditions, 'sunset' => $sunset, 'sunrise' => $sunrise);
}
/**
 * https://www.wunderground.com/weather/api/d/docs?d=resources/phrase-glossary&MR=1
 * maps forcast conditions to one of four gifs we use the terms: cloud,rain,snow,clear
 * @return array
 
 */
function get_weather_conditions_map(){
	$weather_conditions = array(
		'Chance of Flurries' => 'cloud',
		'Chance of Rain' => 'cloud',
		'Chance Rain' => 'cloud',
		'Chance of Freezing Rain' => 'cloud',
		'Chance of Sleet' => 'cloud',
		'Chance of Snow' => 'cloud',
		'Chance of Thunderstorms' => 'cloud',
		'Chance of a Thunderstorm' => 'cloud',
		'Clear' => 'clear',
		'Cloudy' => 'cloud',
		'Flurries' => 'snow',
		'Fog' => 'cloud',
		'Haze' => 'cloud',
		'Mostly Cloudy' => 'cloud',
		'Mostly Sunny' => 'clear',
		'Partly Cloudy' => 'cloud',
		'Partly Sunny' => 'clear',
		'Freezing Rain' => 'rain',
		'Rain' => 'rain',
		'Sleet' => 'snow',
		'Sunny' => 'clear',
		'Snow' => 'snow',
		'Thunderstorms' => 'rain',
		'Thunderstorm' => 'rain',
		'Unknown' => 'cloud',
		'Overcast' => 'cloud',
		'Scattered Clouds' => 'cloud',
	);

	return $weather_conditions;
}