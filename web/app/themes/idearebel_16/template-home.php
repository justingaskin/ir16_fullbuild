<?php
/**
 * Template Name: Home Page Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
<?php
	// Load Home Page As default.
	// Example args
	$args = array('p' => 7,
				  'post_type' => 'page');
	// the query
	$about_us_page_query = new WP_Query($args);
	if($about_us_page_query->have_posts()) {
		$about_us_page_query->the_post();
		echo "<div id='ir-carousel-0' class='home-content about-us-content active-carousel'>";
			get_template_part('templates/content', 'page');
		echo "</div>";
	}
	wp_reset_postdata();
	
	//TODO: REMOVE AND APPLY AJAX FOR EACH PAGE REQUEST
	// $args = array('post_type' => 'workproject',
					// 'orderby' => 'meta_value_num',
					// 'meta_key'  => 'display_order',
					// 'order' => 'ASC');
	// $slider_query = new \WP_Query($args);
	$slider_query->rewind_posts();
	$carousel_position = 0;
	while($slider_query->have_posts()) {
		$slider_query->the_post();
		echo "<div id='ir-carousel-" . ++$carousel_position . "' class='inactive-carousel'>";
		  echo "<article " . $slider_query->post_class() . ">";
		    echo '<div class="entry-content container ir-project-page-container">';
		       the_content();
		    echo '</div>';
		  echo '</article>';
	  		Roots\Sage\Extras\display_project_navigation();
		echo "</div>";
	}
	wp_reset_postdata();
?>