<?php
/**
 * Template Name: Services Page Template
 */
?>
<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'services'); ?>
<?php endwhile; ?>