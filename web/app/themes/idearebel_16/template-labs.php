<?php
/**
 * Template Name: Labs Page Template
 */
?>
<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'labs'); ?>
<?php endwhile; ?>